<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201019104228 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');


        $this->addSql('CREATE TABLE message_chat (message_id INT NOT NULL, chat_id INT NOT NULL, PRIMARY KEY(message_id, chat_id))');
        $this->addSql('CREATE INDEX IDX_CC086973537A1329 ON message_chat (message_id)');
        $this->addSql('CREATE INDEX IDX_CC0869731A9A7125 ON message_chat (chat_id)');
        $this->addSql('ALTER TABLE message_chat ADD CONSTRAINT FK_CC086973537A1329 FOREIGN KEY (message_id) REFERENCES message (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE message_chat ADD CONSTRAINT FK_CC0869731A9A7125 FOREIGN KEY (chat_id) REFERENCES chat (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE message DROP CONSTRAINT fk_b6bd307f1a9a7125');
        $this->addSql('DROP INDEX idx_b6bd307f1a9a7125');
        $this->addSql('ALTER TABLE message DROP chat_id');

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP TABLE message_chat');

        $this->addSql('ALTER TABLE message ADD chat_id INT NOT NULL');
        $this->addSql('ALTER TABLE message ADD CONSTRAINT fk_b6bd307f1a9a7125 FOREIGN KEY (chat_id) REFERENCES chat (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_b6bd307f1a9a7125 ON message (chat_id)');
    }
}
