<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200924103057 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');


        $this->addSql('ALTER TABLE filter ADD slug VARCHAR(128) DEFAULT NULL');
        $this->addSql('ALTER TABLE filter ALTER sort_order SET DEFAULT \'0\'');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_7FC45F1D989D9B62 ON filter (slug)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP INDEX UNIQ_7FC45F1D989D9B62');
        $this->addSql('ALTER TABLE filter DROP slug');
        $this->addSql('ALTER TABLE filter ALTER sort_order DROP DEFAULT');

    }
}
