<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200729113123 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');


        $this->addSql('CREATE TABLE product_type_category (product_type_id INT NOT NULL, category_id INT NOT NULL, PRIMARY KEY(product_type_id, category_id))');
        $this->addSql('CREATE INDEX IDX_115E22CF14959723 ON product_type_category (product_type_id)');
        $this->addSql('CREATE INDEX IDX_115E22CF12469DE2 ON product_type_category (category_id)');
        $this->addSql('ALTER TABLE product_type_category ADD CONSTRAINT FK_115E22CF14959723 FOREIGN KEY (product_type_id) REFERENCES product_type (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE product_type_category ADD CONSTRAINT FK_115E22CF12469DE2 FOREIGN KEY (category_id) REFERENCES category (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP TABLE product_type_category');
    }
}
