<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200728161628 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');


        $this->addSql('CREATE TABLE filter (id INT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE filter_product (filter_id INT NOT NULL, product_id INT NOT NULL, PRIMARY KEY(filter_id, product_id))');
        $this->addSql('CREATE INDEX IDX_64DA73C8D395B25E ON filter_product (filter_id)');
        $this->addSql('CREATE INDEX IDX_64DA73C84584665A ON filter_product (product_id)');
        $this->addSql('ALTER TABLE filter_product ADD CONSTRAINT FK_64DA73C8D395B25E FOREIGN KEY (filter_id) REFERENCES filter (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE filter_product ADD CONSTRAINT FK_64DA73C84584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE filter_product DROP CONSTRAINT FK_64DA73C8D395B25E');
        $this->addSql('DROP TABLE filter');
        $this->addSql('DROP TABLE filter_product');

    }
}
