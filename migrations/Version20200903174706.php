<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200903174706 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');


        $this->addSql('ALTER TABLE filter_values ALTER id DROP DEFAULT');
        $this->addSql('ALTER TABLE filter_values ALTER value SET NOT NULL');
        $this->addSql('ALTER TABLE filter_values ALTER value_id SET NOT NULL');
        $this->addSql('ALTER TABLE filter_values ADD CONSTRAINT FK_178C1298D395B25E FOREIGN KEY (filter_id) REFERENCES filter (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_178C1298D395B25E ON filter_values (filter_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE filter_values DROP CONSTRAINT FK_178C1298D395B25E');
        $this->addSql('DROP INDEX IDX_178C1298D395B25E');
        $this->addSql('CREATE SEQUENCE filter_values_id_seq');
        $this->addSql('SELECT setval(\'filter_values_id_seq\', (SELECT MAX(id) FROM filter_values))');
        $this->addSql('ALTER TABLE filter_values ALTER id SET DEFAULT nextval(\'filter_values_id_seq\')');
        $this->addSql('ALTER TABLE filter_values ALTER value DROP NOT NULL');
        $this->addSql('ALTER TABLE filter_values ALTER value_id DROP NOT NULL');

    }
}
