<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201015202803 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE chat_id_seq INCREMENT BY 1 MINVALUE 1 START 1');

        $this->addSql('CREATE TABLE chat (id INT NOT NULL, to_user_id INT NOT NULL, from_user_id INT NOT NULL, product_id INT NOT NULL, slug VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_659DF2AA29F6EE60 ON chat (to_user_id)');
        $this->addSql('CREATE INDEX IDX_659DF2AA2130303A ON chat (from_user_id)');
        $this->addSql('CREATE INDEX IDX_659DF2AA4584665A ON chat (product_id)');
        $this->addSql('ALTER TABLE chat ADD CONSTRAINT FK_659DF2AA29F6EE60 FOREIGN KEY (to_user_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chat ADD CONSTRAINT FK_659DF2AA2130303A FOREIGN KEY (from_user_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chat ADD CONSTRAINT FK_659DF2AA4584665A FOREIGN KEY (product_id) REFERENCES product (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE message_product');
        $this->addSql('ALTER TABLE message DROP CONSTRAINT fk_b6bd307f2130303a');
        $this->addSql('ALTER TABLE message DROP CONSTRAINT fk_b6bd307f29f6ee60');
        $this->addSql('DROP INDEX idx_b6bd307f2130303a');
        $this->addSql('DROP INDEX idx_b6bd307f29f6ee60');
        $this->addSql('ALTER TABLE message ADD author_id INT NOT NULL');
        $this->addSql('ALTER TABLE message ADD chat_id INT NOT NULL');
        $this->addSql('ALTER TABLE message DROP from_user_id');
        $this->addSql('ALTER TABLE message DROP to_user_id');
        $this->addSql('ALTER TABLE message DROP slug');
        $this->addSql('ALTER TABLE message ADD CONSTRAINT FK_B6BD307FF675F31B FOREIGN KEY (author_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE message ADD CONSTRAINT FK_B6BD307F1A9A7125 FOREIGN KEY (chat_id) REFERENCES chat (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_B6BD307FF675F31B ON message (author_id)');
        $this->addSql('CREATE INDEX IDX_B6BD307F1A9A7125 ON message (chat_id)');

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE message DROP CONSTRAINT FK_B6BD307F1A9A7125');
        $this->addSql('DROP SEQUENCE chat_id_seq CASCADE');

        $this->addSql('CREATE TABLE message_product (message_id INT NOT NULL, product_id INT NOT NULL, PRIMARY KEY(message_id, product_id))');
        $this->addSql('CREATE INDEX idx_43c23ea1537a1329 ON message_product (message_id)');
        $this->addSql('CREATE INDEX idx_43c23ea14584665a ON message_product (product_id)');
        $this->addSql('ALTER TABLE message_product ADD CONSTRAINT fk_43c23ea1537a1329 FOREIGN KEY (message_id) REFERENCES message (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE message_product ADD CONSTRAINT fk_43c23ea14584665a FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE chat');
        $this->addSql('ALTER TABLE message DROP CONSTRAINT FK_B6BD307FF675F31B');
        $this->addSql('DROP INDEX IDX_B6BD307FF675F31B');
        $this->addSql('DROP INDEX IDX_B6BD307F1A9A7125');
        $this->addSql('ALTER TABLE message ADD from_user_id INT NOT NULL');
        $this->addSql('ALTER TABLE message ADD to_user_id INT NOT NULL');
        $this->addSql('ALTER TABLE message ADD slug VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE message DROP author_id');
        $this->addSql('ALTER TABLE message DROP chat_id');
        $this->addSql('ALTER TABLE message ADD CONSTRAINT fk_b6bd307f2130303a FOREIGN KEY (from_user_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE message ADD CONSTRAINT fk_b6bd307f29f6ee60 FOREIGN KEY (to_user_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_b6bd307f2130303a ON message (from_user_id)');
        $this->addSql('CREATE INDEX idx_b6bd307f29f6ee60 ON message (to_user_id)');
    }
}
