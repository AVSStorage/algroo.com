import {Module, Mutation, Action} from 'vuex-module-decorators'
import axios, {AxiosError, AxiosResponse, AxiosRequestConfig} from 'axios'
import ErrorHandlerVuexClass from './ErrorClass'
import {FormFieldType} from "./formDataModel";

interface ApiError {
    config: AxiosRequestConfig;
    code?: string;
    response?: AxiosResponse<string>;
    isAxiosError: boolean;
    toJSON: () => object;
}

interface ServerData {
    data: string
}

export type ChoiceType = {
    id: number,
    name: string,
    lvl: number
}

export type FiltersType = Array<{ value: Array<string> } & ChoiceType>;
export type ChoicesType = Array<ChoiceType>;

export type CategoryData = {
    choices: ChoicesType,
    types?: ChoiceType[],
    filters?: FiltersType,
    parent?: boolean,
    selectedFilters?: SelectedFiltersType,
    lvl: number
}

export type SelectedFiltersType =   Array<{id: number, value: number, type:number}>
export type ProductTypeType = Array<Exclude<ChoiceType, "lvl">>

export type GetCategoryDataType = {
    id: number,
    lvl: number
}

export type FormDataType = {} | FormFieldType & { choices?: ChoiceType[], lvl: number, form_name: string }

@Module({namespaced: true})
class CategoryModel extends ErrorHandlerVuexClass {
    get choices(): Array<ChoiceType> {
        return this._choices;
    }

    get types(): Array<Exclude<ChoiceType, "lvl">> {
        return this._types;
    }

    get filters(): Array<{ value: Array<string> } & ChoiceType> {
        return this._filters;
    }
    // @ts-ignore
    private _choices: ChoicesType = [];
    public categoryPostData = [];
    private _types: ProductTypeType = [];
    private _filters: FiltersType = [];
    public categoryFormData: FormDataType = {};
    public lvl = 0;
    public selectedCategories: Array<GetCategoryDataType> = [];
    public selectedFilters: SelectedFiltersType = [];
    public models: Array<{id: number, name: string}> = [];

    @Mutation
    public setCategoryFormData(formData: FormDataType) {
        this.categoryFormData = formData;

    }


    @Mutation
    public setFiltersAndLvl({filters, lvl}: { filters: FiltersType, lvl: number }) {
        this.lvl = lvl;
        this._filters.splice(0, 1);
        this._filters.push(filters as any || []);
    }

    @Mutation
    public updateChoices(lvl: number) {
        if (this._choices.length > lvl) {
            this._choices.splice(lvl + 1, this._choices.length - lvl);
        }
    }

    @Mutation
    public setTypes(types: ChoiceType[]) {
        this._types = types || [];
    }

    @Mutation
    public setSelectedFilters(filters: SelectedFiltersType) {
        console.log(filters);
        this.selectedFilters = filters;
    }

    @Mutation
    public initializeSelectedChoices({choices, lvl} : {choices: ChoicesType, lvl: number}) {

        if (this._choices[lvl] !== undefined) {

            this._choices.splice(lvl, 1,choices as any || []);
            if (this._choices.length > lvl) {
                this._choices.splice(lvl + 1, this._choices.length - lvl);
            }

        } else if (lvl === 1) {
            // @ts-ignore
            if (choices.length > 1) {
                this._choices = choices || [];
            } else {
                this._choices.splice(1, 1, choices as any || []);
            }
        } else {
            this._choices = choices || [];
        }
    }

    @Action({ rawError: true})
    public updateCategory({data, init}: { data: CategoryData & {selectedCategories: Array<number>, models?: Array<{ id: number, name: string}>} , init: boolean}) {


        if (init) {
            this.context.commit('setCategoryFormData', data);
        }
        if (data.hasOwnProperty('selectedFilters')) {
            this.context.commit('setSelectedFilters', data.selectedFilters);

        }

        if (data.hasOwnProperty('models')) {
            this.context.commit('addModels', {data: data.models});

        }

        if (!data.hasOwnProperty('parent')) {
            if (data.hasOwnProperty('selectedCategories')) {

                this.context.commit('setSelectedCategories', data.selectedCategories);
                this.context.commit('initializeSelectedChoices', {choices:data.choices, lvl: data.lvl});
                this.context.commit('setTypes', data.types);
            } else {
                this.context.commit('setChoices', data);
            }
            if (data.lvl < 2 && !data.hasOwnProperty('selectedCategories')) {

                this.context.commit('setTypes', data.types);
            }
            this.context.commit('setFiltersAndLvl', {filters: data.filters, lvl: data.lvl});
        } else {
            if (this.context.getters['choices'][data.lvl] !== undefined) {
                this.context.commit('updateChoices', data.lvl);
            }
            // @ts-ignore
            if (data.hasOwnProperty('types') && data.types.length > 0) {
                this.context.commit('setTypes', data.types);
            }
            this.context.commit('setFiltersAndLvl', {filters: data.filters, lvl: data.lvl});
        }
    }

    @Mutation
    public setChoices(data: CategoryData ) {
        // @ts-ignore

        if (this._choices[data.lvl] !== undefined) {

            this._choices.splice(data.lvl, 1, data.choices as any || []);
            if (this._choices.length > data.lvl) {
                this._choices.splice(data.lvl + 1, this._choices.length - data.lvl);
            }

        } else if (data.lvl === 1) {
                this._choices.splice(1, 1, data.choices as any || []);
        } else {
                this._choices.push(data.choices as any || [])
        }


        let index = this.selectedCategories.findIndex((category: GetCategoryDataType) => category.lvl === data.lvl)

        // if (index === -1) {
        //     // @ts-ignore
        //     this.selectedCategories.push({ id: data.choices[0].id, lvl: data.lvl})
        // }
        if (index !== -1) {

            // @ts-ignore
            this.selectedCategories.splice(index, 1, { id: data.choices[0].id, lvl: data.lvl});

        }

    }

    @Mutation
    public setCategory(category: number) {
        // @ts-ignore
        this.categoryPostData.category = category
    }

    @Mutation
    public setSelectedFilter({id, value, type}: {id: number, value: number, type: number}) {

        let index = this.selectedFilters.findIndex((filter: {id: number, value: number}) => filter.id === id );
        console.log(index,id, value, type);
        if (index === -1) {
            this.selectedFilters.push({id : id, value: value, type: type ? type: 0})
        } else {

            this.selectedFilters.splice(index, 1, {id : id, value: value, type: type ? type : 0});
        }
    }

    @Action
    public setFilter(filter: {id: number, value: number, type: number}) {
        this.context.commit('setSelectedFilter', filter);
    }


    @Mutation
    public updateSelectedFilterMutation(id: number) {
        const index = this.selectedFilters.findIndex((item: {id: number, value: number}) => item.id === id);
        if (index !== -1) {
            this.selectedFilters.splice(index, 1);
        }
    }

    @Action
    public updateSelectedFilter(id: number) {
        this.context.commit('updateSelectedFilterMutation', id);
    }

    @Mutation
    public setSelectedCategories(value: GetCategoryDataType | Array<number>) {


        if (Array.isArray(value)) {
            value.forEach((item: number, index: number) => {
                this.selectedCategories.push({
                    id: item,
                    lvl: index
                })
            })
        } else {
            // @ts-ignore
            let index = this.selectedCategories.findIndex((category: GetCategoryDataType) => category.lvl === value.lvl)

            if (index === -1) {
                // @ts-ignore
                this.selectedCategories.push(value)
            } else {
                if (value.id === 0) {
                    const myData = this.selectedCategories.filter((category : GetCategoryDataType) => {
                        return category.lvl > value.lvl
                    });


                    myData.forEach((data) => {
                        const index = this.selectedCategories.findIndex(ind => ind === data )
                        this.selectedCategories.splice(index, 1)
                        // @ts-ignore
                        this._choices.splice(data.lvl, 1)
                    });

                    let ind = value.lvl + 1;


                    while (ind <= this._choices.length) {
                        this._choices.splice(ind, 1)
                        ind++;
                    }


                }
                // @ts-ignore
                this.selectedCategories.splice(index, 1, value);

            }
        }
    }

    @Action({rawError: true})
    public setCategoryPostData({data}: { data: GetCategoryDataType }) {

        this.context.commit('setCategory', data.id);
        this.context.commit('setSelectedCategories', data);
        if (data.id > 0) {
            const formData = new FormData();
            formData.append('id', data.id.toString());
            formData.append('lvl', data.lvl.toString());

            this.context.dispatch('getCategory', {data: formData});
        }

    }

    @Mutation
    public addModels(models: Array<{id: number, name: string}>) {
        console.log(models);
        // @ts-ignore
        this.models = models.data;
    }

    @Action
    public setFilters(data: string) {
        // console.log(JSON.parse(data));
        this.context.commit('addModels', {data: JSON.parse(data)});
    }

    @Action({rawError: true})
    public async getFilters(formDataSet: {value: number, id : number}) {
        console.log(formDataSet);

        //@ts-ignore
        let formData = new FormData();
        //@ts-ignore
        formData.append('value_id',  formDataSet.value);
        axios.request<ServerData>({
            url: '/filter/models/get',
            method: 'POST',
            data: formData,
            headers: {'X-Requested-With': 'XMLHttpRequest'},
            transformResponse: (r) => {
                return r
            }
        }).then((response: AxiosResponse) => {
            // `response` is of type `AxiosResponse<ServerData>`
            const {data} = response;

            this.context.dispatch('setFilters', data);
            this.context.dispatch('setFilter', {id: Number(formDataSet.id) , value: Number(formDataSet.value)});
        }).catch((error: AxiosError<ApiError>) => {
            this.context.commit('setError', error.message)
        })
    }

    @Action({rawError: true})
    public async fetchCategoryFormData() {
        axios.request<ServerData>({
            url: '/category/add-form/get',
            method: 'GET',
            headers: {'X-Requested-With': 'XMLHttpRequest'},
            transformResponse: (r) => {
                return r
            }
        }).then((response: AxiosResponse) => {
            // `response` is of type `AxiosResponse<ServerData>`
            const {data} = response;

            this.context.dispatch('updateCategory', {data: JSON.parse(data), init: true});
        }).catch((error: AxiosError<ApiError>) => {
            this.context.commit('setError', error.message)
        })
    }

    @Action({rawError: true})
    public async getCategory({data}: { data: GetCategoryDataType }) {
        axios.request<ServerData>({
            url: '/category/get',
            method: 'POST',
            data: data,
            headers: {'X-Requested-With': 'XMLHttpRequest'},
            transformResponse: (r) => {
                return r
            }
        }).then((response: AxiosResponse) => {
            // `response` is of type `AxiosResponse<ServerData>`
            const {data} = response;

            this.context.dispatch('updateCategory', {data: JSON.parse(data), init: false});

        }).catch((error: AxiosError<ApiError>) => {
            this.context.commit('setError', error.message)
        })
    }

}

export default CategoryModel