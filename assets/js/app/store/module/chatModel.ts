import {Module, Mutation, Action} from 'vuex-module-decorators'
// @ts-ignore
import * as autobahn  from '../../../../bundles/goswebsocket/js/vendor/autobahn.min.js';
// @ts-ignore
import * as WS from '../../../../bundles/goswebsocket/js/websocket.js';
import axios, {AxiosError, AxiosResponse, AxiosRequestConfig} from 'axios'
import ErrorHandlerVuexClass from './ErrorClass'

interface ApiError {
    config: AxiosRequestConfig;
    code?: string;
    response?: AxiosResponse<string>;
    isAxiosError: boolean;
    toJSON: () => object;
}

interface ServerData {
    data: string
}

export type MessageType = {
    id: number,
    text: string,
    is_read: boolean,
    to_user : number,
    from_user: number
}


let sendMessageWs : (message : MessageType) => void;

@Module({namespaced: true})
class ChatModel extends ErrorHandlerVuexClass {
    public messages : MessageType[] = [];
    public productId : number | null = null;
    public conversationId : string | null = null;
    fromUser : number | null = null;
    toUser : number | null = null;
    loaded = false;


    @Mutation
    addMessage (message: MessageType) {

        if (this.messages) {
            this.messages.push(message);
        } else {
            this.messages = [message]
        }
    }

    @Mutation
    addMessages (messages: MessageType[]) {

        this.messages = messages;
        this.loaded = true;
    }

    @Mutation
    markMessageAsRead(message: MessageType) {
        const index = this.messages.indexOf(message);
        console.log(this.messages[index]);
        this.messages.splice(index, 1, {...this.messages[index], is_read: true})
    }

    @Action
    setSocket() {
        var websocket = WS.connect("ws://127.0.0.1:1337");

        websocket.on("socket/connect",  (session : {
            publish: (url : string, payload: {} ) => void,
            subscribe: (url : string, payload: {} ) => void,
            unsubscribe: (url : string ) => void,
        }) =>  {
            session.subscribe(`app/chat/${this.productId}/${this.toUser}`,  (uri: string, payload : {msg: string}) => {
                console.log("Received message", payload, payload.msg.hasOwnProperty('id'));
                if(payload.msg.hasOwnProperty('id')){
                    this.context.dispatch('messageReceived',payload.msg)
                }
            });

            // session.call("sample/sum", {"term1": 2, "term2": 5}).then(
            //     function (result) {
            //         console.log("RPC Valid!", result);
            //     },
            //     function (error, desc) {
            //         console.log("RPC Error", error, desc);
            //     }
            // );

            // session.publish(`app/chat/${this.user}`, {msg: "This is a message!"});
            //
            // session.publish(`app/chat/${this.user}`, {msg: "I'm leaving, I will not see the next message"});
            //
            // session.unsubscribe(`app/chat/${this.user}`);
            //
            // session.publish(`app/chat/${this.user}`, {msg: "I won't see this"});

            // session.subscribe(`app/chat/${this.user}`, function (uri : string, payload) {
            //     console.log("Received message", payload.msg);
            // });

            sendMessageWs = (message : MessageType) => {
                session.publish(`app/chat/${this.productId}/${this.toUser}`, message);
                return true;
            }
        });

        websocket.on("socket/disconnect", function (error: {reason: string, code: number}) {
            //error provides us with some insight into the disconnection: error.reason and error.code

            console.log("Disconnected for " + error.reason + " with code " + error.code);
        });

    }

    @Action
    async sendMessage (content: string){
        const formData = new FormData();
        formData.append('product', String(this.productId));
        formData.append('content', content);
        formData.append('channel', String(this.conversationId));
        formData.append('from_user', String(this.fromUser));
        formData.append('to_user', String(this.toUser));
        return axios.request<ServerData>({
            url: '/api/conversations/' + this.toUser,
            method: 'POST',
            data: formData,
            headers: {'X-Requested-With': 'XMLHttpRequest'},
            transformResponse: (r) => {
                return r
            }
        }).then((response: AxiosResponse) => {
            // `response` is of type `AxiosResponse<ServerData>`
            let {data} = response;
            data = JSON.parse(data);

            console.log(data);
           // this.context.commit('addMessage',  data.message);
           sendMessageWs(data.message)
           // this.context.commit('markMessageAsRead',  data.message);
          //  sendMessageWs(data.message, this.user)
        }).catch((error: AxiosError<ApiError>) => {
            this.context.commit('setError', error.message)
        });


    }

    @Action
    getMessages(messages: MessageType[]) {
        this.context.commit('addMessages',  messages );

    }

    @Mutation
    setFromUser(user: number) {
        this.fromUser = user;
    }

    @Mutation
    setToUser(user: number) {
        this.toUser = user;
    }

    @Mutation
    setConversationId(conversationId: string) {
        this.conversationId = conversationId;
    }

    @Mutation
    setProductId(productId: number) {
        this.productId = productId;
    }

    @Action({ rawError: true})
    async loadMessages(){
        // if (!context.getters.conversation(conversationId).loaded) {
        return axios.request<ServerData>({
            url: '/api/conversations/' + this.conversationId,
            headers: {'X-Requested-With': 'XMLHttpRequest'},
            transformResponse: (r) => {
                return r
            }
        }).then((response: AxiosResponse) => {
            // `response` is of type `AxiosResponse<ServerData>`
            let {data} = response;
            data = JSON.parse(data);
            this.context.dispatch('getMessages', data.message)
            this.context.dispatch('markAsRead')

        }).catch((error: AxiosError<ApiError>) => {
            this.context.commit('setError', error.message)
        });

    }

    @Action({ rawError: true})
     messageReceived(message : MessageType){
        console.log(message);
        this.context.commit('addMessage', message)
        // if(!context.state.openedConversation.includes(message.from_user.id) || document.hidden){
        //     context.commit('incrementUnread', message.from_user.id)
        // }else{
          // this.context.dispatch('markAsRead', message)
        // }
    }



    @Action({ rawError: true})
    async  markAsRead () {

        this.messages.forEach(message => {
            if ((message.from_user === this.fromUser) && !message.is_read ) {


                let formData = new FormData();
                formData.append('id', String(message.id));
                axios.request<ServerData>({
                    url: '/api/messages/read/' + message.id,
                    method: 'POST',
                    data: formData,
                    headers: {'X-Requested-With': 'XMLHttpRequest'},
                    transformResponse: (r) => {
                        return r
                    }
                }).then((response: AxiosResponse) => {
                    // `response` is of type `AxiosResponse<ServerData>`
                    const {data} = response;
                    this.context.commit('markMessageAsRead',  JSON.parse(data.message))
                }).catch((error: AxiosError<ApiError>) => {
                    this.context.commit('setError', error.message)
                });
            }
        });

    }
}

export default ChatModel