import { VuexModule, Mutation, Action } from 'vuex-module-decorators'


abstract class ErrorHandlerVuexClass extends VuexModule {
    protected error: string | boolean = false
    @Mutation
    protected setError (error: string) {
        this.error = error
    }

    @Mutation
    protected reset () {
        this.error = ''
    }

    @Action
    protected resetError() {
        this.context.commit('reset')
    }
}

export default ErrorHandlerVuexClass
