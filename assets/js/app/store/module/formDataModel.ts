// store/modules/user.ts
import { Module, Mutation, Action } from 'vuex-module-decorators'
import axios, { AxiosError, AxiosResponse, AxiosRequestConfig } from 'axios'
import ErrorHandlerVuexClass from './ErrorClass'


interface ApiError {
  config: AxiosRequestConfig;
  code?: string;
  response?: AxiosResponse<string>;
  isAxiosError: boolean;
  toJSON: () => object;
}

export type FormFieldType = {
    id: string,
    full_name: string,
    label?: string,
    label_attr: Array<{ class: string }>,
    required: boolean,
    attr: { class?: string, placeholder?: string},
    value?: string
}

export type FormDataType = Record<string, FormFieldType>

interface ServerData {
   data: string
}

@Module({ namespaced: true })
class FormDataModel extends ErrorHandlerVuexClass {
  public formData: FormDataType = {};
  public formResetPassword: FormDataType = {};
  public successMessage = '';
  public isValidEmail = true;
  public ulEmail = '';
  public  personEmail = '';
  @Mutation
  public  setFormData(formData : FormDataType) {
      this.formData = formData;
  }

    @Mutation
    public  setFormResetPassword(formResetPassword : FormDataType) {
        this.formResetPassword = formResetPassword;
    }

    @Mutation
    public setSuccessMessage(message: string) {
        this.personEmail = '';
      this.successMessage = message;
    }

    @Mutation
    public setCheckEmail() {
        this.isValidEmail = false;
    }

    @Mutation
    public resetEmail() {
        this.isValidEmail = true;

    }

    @Mutation
    public setUlEmail(value: string) {

        this.ulEmail = value;

    }

    @Mutation
    public setNotValidEmail(email: string) {
      console.log(email);
      this.personEmail = email;
    }

    @Action({ rawError: true })
    public updateEmail(value: string, name: string) {
        this.context.commit('setUlEmail', value);

    }

    @Action({ rawError: true })
    public resetInvalidEmail() {
        this.context.commit('resetEmail');
    }

    @Action({ rawError: true })
    public async fetchResetPasswordData () {
        axios.request<ServerData>({
            url: '/reset-password',
            method: 'GET',
            headers: {'X-Requested-With': 'XMLHttpRequest'},
            transformResponse: (r) => { return r }
        }).then((response : AxiosResponse) => {
            // `response` is of type `AxiosResponse<ServerData>`
            const { data } = response;


            this.context.commit('setFormResetPassword', JSON.parse(data));

        }).catch((error: AxiosError<ApiError>) => {
            this.context.commit('setError', error.message)
        })
    }

    @Action({ rawError: true })
    public  getEmail () {
        return axios.request<ServerData>({
            url: '/api/user/ul/form/email',
            method: 'GET',
            headers: {'X-Requested-With': 'XMLHttpRequest'},
            transformResponse: (r) => { return r }
        }).then((response : AxiosResponse) => {
            // `response` is of type `AxiosResponse<ServerData>`
            const { data } = response;

            this.context.commit('setFormData', JSON.parse(data));
        }).catch((error: AxiosError<ApiError>) => {
            this.context.commit('setError', error.message)
        })
    }

    @Action({ rawError: true })
    public  checkEmail (data: FormData) {
        return axios.request<ServerData>({
            url: '/api/user/email/check',
            method: 'POST',
            data: data,
            headers: {'X-Requested-With': 'XMLHttpRequest', 'Content-Type': 'multipart/form-data'},
            transformResponse: (r) => { return r }
        }).then((response : AxiosResponse) => {
            // `response` is of type `AxiosResponse<ServerData>`
            const { data } = response;

            this.context.commit('resetEmail');
            this.context.commit('reset');
        }).catch((error: AxiosError<ApiError>) => {
            // @ts-ignore
            const { data } = error.response;
            this.context.commit('setCheckEmail');
            this.context.commit('setNotValidEmail',  JSON.parse(data).email);
            this.context.commit('setError', JSON.parse(data).message)
        })
    }

    @Action({ rawError: true })
    public async sendEmail (data: string) {
        axios.request<ServerData>({
            url: '/reset-password',
            method: 'POST',
            data: data,
            headers: {
                'X-Requested-With': 'XMLHttpRequest',
                'Content-Type': 'multipart/form-data'
            },
            transformResponse: (r) => { return r }
        }).then((response : AxiosResponse) => {
            // `response` is of type `AxiosResponse<ServerData>`
            const { data } = response;

            this.context.commit('setSuccessMessage', JSON.parse(data).message)
        }).catch((error: AxiosError<ApiError> ) => {
            // @ts-ignore
            const { data } = error.response;

            this.context.commit('setError', JSON.parse(data).message)
        })
    }


  @Action({ rawError: true })
  public async fetchFormData () {
      axios.request<ServerData>({
          url: '/api/house',
          headers: {'X-Requested-With': 'XMLHttpRequest'},
          transformResponse: (r) => { return r }
      }).then((response : AxiosResponse) => {
          // `response` is of type `AxiosResponse<ServerData>`
          const { data } = response;


          this.context.commit('setFormData', JSON.parse(data))
      }).catch((error: AxiosError<ApiError>) => {

          this.context.commit('setError', error.message)
      })
  }
}
export default FormDataModel
