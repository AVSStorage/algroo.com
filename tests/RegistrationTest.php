<?php

namespace App\Tests;

use Monolog\Test\TestCase;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class RegistrationTest extends WebTestCase
{
    private $serializer;
    protected function setUp()
    {
        parent::setUp();
        $normalizer = new ObjectNormalizer();
        $encoder = new JsonEncoder();
        $this->serializer = new Serializer([$normalizer], [$encoder]);
    }

    /**
     * Before testing disable CSRF protection in ResetPasswordRequestFormType
     * @dataProvider registerData
     */
    public function test_ajax_post_reset_password_route($tested, $expected)
    {
        list($data, $status) = $expected;
        $client = static::createClient();
        $client->xmlHttpRequest('POST','/reset-password',$tested,[],['CONTENT_TYPE' => 'multipart/form-data']);
        $this->assertEquals($status, $client->getResponse()->getStatusCode());
        $this->assertEquals($this->serializer->serialize($data, 'json'), $client->getResponse()->getContent());
    }

    public function registerData(){
        return [
            [['reset_password_request_form' => ['email' => 'alise@mail.ru']], [['message' => 'Такого пользователя не существует'],Response::HTTP_NOT_ACCEPTABLE ]],
            [['reset_password_request_form' => ['email' => 'alise-fox@inbox.ru']], [['message' => 'Данные были обновлены. Проверьте Вашу почту.'], Response::HTTP_OK]],
        ];
    }

}
