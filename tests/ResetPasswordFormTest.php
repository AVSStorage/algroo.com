<?php

namespace App\Tests;

use App\Form\ResetPasswordRequestFormType;
use App\Helpers\FormFieldHelper;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Form\Test\TypeTestCase;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class ResetPasswordFormTest extends TypeTestCase
{
    private $serializer;
    protected function setUp() : void
    {
        parent::setUp();
        $normalizer = new ObjectNormalizer();
        $encoder = new JsonEncoder();
        $this->serializer = new Serializer([$normalizer], [$encoder]);
    }

    public function test_get_reset_password_route()
    {
        $form = $this->factory->create(ResetPasswordRequestFormType::class);
        $view = $form->createView();
        $fields = $view->children;
        $fieldHelper = new FormFieldHelper($fields,[ 'id','full_name', 'label',  'label_attr' , 'required', 'attr']);
        $this->assertIsArray($fieldHelper->extractFields());
        $this->assertContains([ 'id','full_name', 'label',  'label_attr' , 'required', 'attr'], array_values($fieldHelper->extractFields()['email']));
    }
}
