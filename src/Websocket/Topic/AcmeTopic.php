<?php

namespace App\Websocket\Topic;

use App\Repository\UserRepository;
use Gos\Bundle\WebSocketBundle\Client\ClientManipulatorInterface;
use Gos\Bundle\WebSocketBundle\Router\WampRequest;
use Gos\Bundle\WebSocketBundle\Topic\TopicInterface;
use Ratchet\ConnectionInterface;
use Ratchet\Wamp\Topic;
use Symfony\Contracts\Translation\TranslatorInterface;

final class AcmeTopic implements TopicInterface
{

    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var TranslatorInterface
     */
    private $translator;

    public function __construct(UserRepository $userRepository, TranslatorInterface $translator, ClientManipulatorInterface $clientManipulator)
    {
        $this->userRepository = $userRepository;
        $this->translator = $translator;
    }

    /**
     * This will receive any Subscription requests for this topic.
     *
     * @param ConnectionInterface $connection
     * @param Topic $topic
     * @param WampRequest $request
     *
     * @return void
     */
    public function onSubscribe(ConnectionInterface $connection, Topic $topic, WampRequest $request): void
    {

        // This will broadcast the message to ALL subscribers of this topic.
        $userName = $this->userRepository->findOneBy(['id' => $request->getAttributes()->get('user')])->getUserIndividual()->getName();
        $topic->broadcast(['msg' => $this->translator->trans('Пользователь '.$userName.' онлайн ')]);
    }

    /**
     * This will receive any UnSubscription requests for this topic.
     *
     * @param ConnectionInterface $connection
     * @param Topic $topic
     * @param WampRequest $request
     *
     * @return void
     */
    public function onUnSubscribe(ConnectionInterface $connection, Topic $topic, WampRequest $request): void
    {
        //this will broadcast the message to ALL subscribers of this topic.
        $userName = $this->userRepository->findOneBy(['id' => $request->getAttributes()->get('user')])->getUserIndividual()->getName();
        $topic->broadcast(['msg' => $this->translator->trans('Пользователь '.$userName.' вышел ')]);
    }

    /**
     * This will receive any Publish requests for this topic.
     *
     * @param ConnectionInterface $connection
     * @param Topic $topic
     * @param WampRequest $request
     * @param mixed $event The event data
     * @param array $exclude
     * @param array $eligible
     *
     * @return void
     */
    public function onPublish(
        ConnectionInterface $connection,
        Topic $topic,
        WampRequest $request,
        $event,
        array $exclude,
        array $eligible
    ): void {
        /*
            $topic->getId() will contain the FULL requested uri, so you can proceed based on that

            if ($topic->getId() == "acme/channel/shout")
               //shout something to all subs.
        */

        $topic->broadcast(
            [
                'msg' => $event,
            ]
        );
    }

    /**
     * Like RPC is will use to prefix the channel
     *
     * @return string
     */
    public function getName(): string
    {
        return 'acme.topic';
    }
}
