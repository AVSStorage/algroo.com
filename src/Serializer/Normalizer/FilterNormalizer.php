<?php

namespace App\Serializer\Normalizer;

use App\Entity\Filter;
use Symfony\Component\Serializer\Normalizer\CacheableSupportsMethodInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class FilterNormalizer implements NormalizerInterface, CacheableSupportsMethodInterface
{
    private $normalizer;

    public function __construct(ObjectNormalizer $normalizer)
    {
        $this->normalizer = $normalizer;
    }

    public function normalize($objects, $format = null, array $context = array()): array
    {
        $data = [];
        foreach ($objects as $object) {
            $data[] = [
                'id' => $object->getId(),
                'name' => $object->getName(),
                'values' => $object->getValues(),
                'type' => $object->getType()
            ];
        }

        return $data;
    }

    public function supportsNormalization($data, $format = null): bool
    {
        return $data instanceof Filter;
    }

    public function hasCacheableSupportsMethod(): bool
    {
        return true;
    }
}
