<?php

namespace App\Serializer\Normalizer;

use Symfony\Component\Form\FormView;
use Symfony\Component\Serializer\Normalizer\CacheableSupportsMethodInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class FormNormalizer implements NormalizerInterface, CacheableSupportsMethodInterface
{
    private $normalizer;

    public function __construct(ObjectNormalizer $normalizer)
    {
        $this->normalizer = $normalizer;
    }

    public function normalize($object, $format = null, array $context = array()): array
    {
        $neededFieldsArray = $context['neededFieldsArray'];
        $fields = $context['fields'];

        $extractedFieldsArray = [];
        foreach ($fields as $key => $field) {


            foreach ($neededFieldsArray as $neededField) {

                if (isset( $field->vars[$neededField])) {

                    $extractedFieldsArray[$key][$neededField] = $field->vars[$neededField];

                }
            }


            if ($key === '_token') {
                $extractedFieldsArray[$key] = array_merge($extractedFieldsArray[$key], ['value' => $field->vars['value']]);
            }
        }
        return $extractedFieldsArray;

        // Here: add, edit, or delete some data
    }

    public function supportsNormalization($data, $format = null): bool
    {
        return $data instanceof FormView;
    }

    public function hasCacheableSupportsMethod(): bool
    {
        return true;
    }
}
