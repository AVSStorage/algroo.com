<?php

namespace App\Serializer\Normalizer;

use App\Entity\Category;
use Symfony\Component\Serializer\Normalizer\CacheableSupportsMethodInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class CollectionNormalizer implements NormalizerInterface, CacheableSupportsMethodInterface
{
    private $normalizer;

    public function __construct(ObjectNormalizer $normalizer)
    {
        $this->normalizer = $normalizer;
    }

    public function normalize($objects, $format = null, array $context = array()): array
    {
       // $data = $this->normalizer->normalize($object, $format, $context);
        $data = [];
        foreach ($objects as $object) {
            $data[] = [
                'id' => $object->getId(),
                'name' => $object->getName()
            ];
        }

        return  $data;
    }

    public function supportsNormalization($data, $format = null): bool
    {
        return $data instanceof Category;
    }

    public function hasCacheableSupportsMethod(): bool
    {
        return true;
    }
}
