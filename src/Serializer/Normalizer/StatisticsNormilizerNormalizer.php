<?php

namespace App\Serializer\Normalizer;

use Symfony\Component\Serializer\Normalizer\CacheableSupportsMethodInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class StatisticsNormilizerNormalizer implements NormalizerInterface, CacheableSupportsMethodInterface
{
    private $normalizer;

    public function __construct(ObjectNormalizer $normalizer)
    {
        $this->normalizer = $normalizer;
    }

    public function normalize($object, $format = null, array $context = array()): array
    {
        $data = [];
        $valid = false;
       foreach ($object as $ob) {
           $data[] = $ob[1] ? $ob[1] : null;
       }

        return $data;
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data;
    }

    public function hasCacheableSupportsMethod(): bool
    {
        return true;
    }
}
