<?php


namespace App\Event;


use App\Entity\User;
use Symfony\Contracts\EventDispatcher\Event;

class EndedPremiumStatusProductEvent extends Event
{
    public const NAME = 'product.premium_end';
    private $user;
    /**
     * @var array
     */
    private $products;

    public function __construct(User $user, $products)
    {
        $this->user = $user;
        $this->products = $products;
    }

    /**
     * @return array
     */
    public function getProducts()
    {
        return $this->products;
    }



    public function getEmail() {

        return $this->user->getEmail();

    }
}