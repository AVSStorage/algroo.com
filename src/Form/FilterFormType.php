<?php

namespace App\Form;

use App\Entity\Filter;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class FilterFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Название',
                'label_attr' => [
                    'class' => 'filters__label'
                ],
                'mapped' => true,
                'attr' => [
                    'class' => 'input'
                ],
                'constraints' => new NotBlank()
            ])
            ->add('sort_order', NumberType::class,[
                'label' => 'Порядок сортировки',
                'label_attr' => [
                    'class' => 'filters__label'
                ],
                'mapped' => true,
                'attr' => [
                    'class' => 'input'
                ]
            ])
        ;

        if (!$options['text_filter']) {
            $builder->add('values', ChoiceType::class,[
                'label' => 'Значения фильтра',
                'label_attr' => [
                    'class' => 'filters__label'
                ],
                'required' => false,
                'mapped' => false,
                'attr' => [
                    'class' => 'input'
                ],
                'constraints' => new NotBlank()
            ]);

            $builder->get('values')->resetViewTransformers();
        }

        if ($options['has_category']) {
             $builder
                 ->add('category', TextType::class, [
                'mapped' => false,
                'constraints' => new NotBlank()
            ]);
        }


    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Filter::class,
            'has_category' => false,
            'text_filter' => false
        ]);
    }
}
