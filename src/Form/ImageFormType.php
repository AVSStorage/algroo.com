<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\All;
use Symfony\Component\Validator\Constraints\Image;

class ImageFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('image', FileType::class, [

                'mapped' => false,
                'required' => true,
                'data_class'=> null,
                'multiple' => false,
                'attr'     => [
                    'accept' => 'image/*',
                    'class' => 'input-file'
                ],
//                'error_bubbling' => true,

                'constraints' =>
                    new Image([
//                    'maxRatio' => 1,
//                        'maxRatioMessage' => 'Соотношение сторон некорректно',
                    ])
            ])
        ;



    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => null,
            'csrf_protection'   => false,
        ]);
    }
}
