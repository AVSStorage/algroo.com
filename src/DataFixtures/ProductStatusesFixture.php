<?php

namespace App\DataFixtures;

use App\Entity\Product;
use App\Entity\ProductStatuses;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ProductStatusesFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $repository = $manager->getRepository(Product::class);
        $statuses = ['На модерации','Не прошло модерации'];
        $productStatus = new ProductStatuses();
        $productStatus->setName('Новое');
        $manager->persist($productStatus);

        foreach ($products as $product) {
            $product->setStatus($productStatus);
            $manager->persist($product);
        }

        foreach ($statuses as $status) {
            $productStatus = new ProductStatuses();
            $productStatus->setName($status);
            $manager->persist($productStatus);
        }

        $manager->flush();
    }
}
