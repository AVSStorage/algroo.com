<?php

namespace App\Command;

use App\Entity\ProductStatuses;
use App\Event\EndedPremiumStatusProductEvent;
use App\Event\NeedUpdateProductEvent;
use App\Event\NotActiveProductEvent;
use App\Repository\ProductRepository;
use App\Repository\ProductStatusesRepository;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Psr\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\Router;

class UpdateAdsCommand extends Command
{
    /**
     * @var ProductRepository
     */
    private $productRepository;
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;
    /**
     * @var Router
     */
    private $router;
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var ProductStatusesRepository
     */
    private $productStatusesRepository;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    private $user;

    public function __construct(string $name = null, ProductRepository $productRepository,
                                \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher,
                                EntityManagerInterface $entityManager,
                                ProductStatusesRepository $productStatusesRepository, UrlGeneratorInterface $router, UserRepository $userRepository)
    {
        parent::__construct($name);
        $this->productRepository = $productRepository;
        $this->eventDispatcher = $eventDispatcher;

        $this->router = $router;
        $this->userRepository = $userRepository;
        $this->productStatusesRepository = $productStatusesRepository;
        $this->entityManager = $entityManager;

    }

    protected static $defaultName = 'app:update-ads';

    protected function configure()
    {
        $this
            ->setDescription('Notify users to update ads')
            ->addOption('days', null, InputOption::VALUE_OPTIONAL, 'Number of active ads status days before update', 30)
        ;
    }


    public function getNotificatedProducts($activeStatus) {
        $products = $this->productRepository->findBy(['status' => $activeStatus]);
        $products = new ArrayCollection($products);
        $productsGroupByUser = [];
        $products->map(function($item) use (&$productsGroupByUser) {
            $productsGroupByUser[$item->getUser()->getId()][] = $item;
        });

        return $productsGroupByUser;
    }


    public function makeChangesAfterPeriod($product, $userId) {
        $overdueAdds  = [];
        $premiumAdds = [];
        $notActiveStatus =  $this->productStatusesRepository->findOneBy(['id' => ProductStatuses::STATUS_NOT_ACTIVE]);
        if ($product->getIsPremium()) {
            $premiumAdds[$userId][] = $product;
            $product->setIsPremium(false);
            $product->setStatus($this->productStatusesRepository->findOneBy(['id' => ProductStatuses::STATUS_ACTIVE]));
        } else {
            $overdueAdds[$userId][] = $product;
            $product->setStatus($notActiveStatus);
        }

        $this->entityManager->persist($product);
        $this->entityManager->flush();

        return [$premiumAdds, $overdueAdds];
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $activeStatus =  $this->productStatusesRepository->findOneBy(['id' => ProductStatuses::STATUS_ACTIVE])->getId();

        $productsGroupByUser = $this->getNotificatedProducts($activeStatus);
        $notifyProducts = [];

        foreach ($productsGroupByUser as $userId =>  $userProducts) {
            foreach ($userProducts as $product) {
                $updatedDate = $product->getUpdatedAt();
                if (!$updatedDate) {
                    $updatedDate = $product->getCreatedAt();
                }
                $nowDate = new \DateTime('now');
                $diff = $nowDate->diff($updatedDate);

                if ($diff->days === 25) {
                    $notifyProducts[$userId][] = $product;

                }

                if ($diff->days >= $input->getOption('days')) {

                    list($premiumAdds, $overdueAdds) = $this->makeChangesAfterPeriod($product, $userId);

                }
            }
        }


        if (!empty($premiumAdds)) {
            foreach ($premiumAdds as $userId => $userProducts) {
                $user = $this->userRepository->findOneBy(['id' => $userId]);
                $this->eventDispatcher->dispatch(new EndedPremiumStatusProductEvent($user, $userProducts), EndedPremiumStatusProductEvent::NAME);
            }
        }

        if (!empty($overdueAdds)) {
            foreach ($overdueAdds as $userId => $userProducts) {
                $user = $this->userRepository->findOneBy(['id' => $userId]);
                $this->eventDispatcher->dispatch(new NotActiveProductEvent($user, $userProducts), NotActiveProductEvent::NAME);
            }
        }

        if (!empty($notifyProducts)) {
            foreach ($notifyProducts as $userId => $userProducts) {
                $user = $this->userRepository->findOneBy(['id' => $userId]);
                $this->eventDispatcher->dispatch(new NeedUpdateProductEvent($user, $userProducts), NeedUpdateProductEvent::NAME);
            }
        }



        $io->success('You have a new command! Now make it your own! Pass --help to see your options.');

        return 0;
    }
}
