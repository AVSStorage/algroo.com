<?php


namespace App\Service;


use App\Entity\Category;
use App\Entity\CategoryStatuses;
use App\Entity\Filter;
use App\Entity\FilterValues;
use App\Entity\Product;
use App\Entity\ProductStatuses;
use App\Repository\CategoryRepository;
use App\Repository\FilterRepository;
use App\Repository\FilterValuesRepository;
use App\Repository\ProductRepository;
use App\Repository\ProductStatusesRepository;
use App\Repository\ProductTypeRepository;
use App\Serializer\Normalizer\CollectionNormalizer;
use App\Serializer\Normalizer\FilterNormalizer;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;

class CategoryService
{

    /**
     * @var CollectionNormalizer
     */
    private $collectionNormalizer;
    /**
     * @var FilterNormalizer
     */
    private $filterNormalizer;
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;
    /**
     * @var ProductRepository
     */
    private $productRepository;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var ProductStatusesRepository
     */
    private $productStatusesRepository;
    /**
     * @var ProductTypeRepository
     */
    private $productTypeRepository;
    /**
     * @var FilterValuesRepository
     */
    private $filterValuesRepository;
    /**
     * @var FilterRepository
     */
    private $filterRepository;

    public function __construct(CategoryRepository $categoryRepository,
                                ProductStatusesRepository $productStatusesRepository,
                                ProductTypeRepository $productTypeRepository,
                                FilterRepository $filterRepository,
                                FilterValuesRepository $filterValuesRepository,
                                CollectionNormalizer $collectionNormalizer, ProductRepository $productRepository, EntityManagerInterface $entityManager, FilterNormalizer $filterNormalizer)
    {

        $this->collectionNormalizer = $collectionNormalizer;
        $this->filterNormalizer = $filterNormalizer;
        $this->categoryRepository = $categoryRepository;
        $this->productRepository = $productRepository;
        $this->entityManager = $entityManager;
        $this->productStatusesRepository = $productStatusesRepository;
        $this->productTypeRepository = $productTypeRepository;
        $this->filterValuesRepository = $filterValuesRepository;
        $this->filterRepository = $filterRepository;
    }

    public function addNewCategory($name, $lvl)
    {

    }

    public function updateProductCategory($id, $parentCategory)
    {
        $product = $this->productRepository->findOneBy(['category' => $id]);
        if ($product) {
            $product->setCategory($parentCategory);
            if ($product->getStatus()->getId() === ProductStatuses::STATUS_NEW) {
                $product->setStatus($this->productStatusesRepository->findOneBy(['id' => ProductStatuses::STATUS_NOT_MODERATED]));
                $product->setReason('Категория не прошла модерацию');
            }
            $this->entityManager->persist($product);
        }
    }

    public function loopProducts($products)
    {
        $ids = [];
        $filters = [];
        foreach ($products as $product) {
            $ids[] = $product->getId();
            if ($product->getProductFilters()->count() > 0) {

                $productFilters = $product->getProductFilters()->getValues();
                foreach ($productFilters as $productFilter) {
                    if ($product->getStatus()->getId() === ProductStatuses::STATUS_ACTIVE) {
                        $filter = $this->filterRepository->findOneBy(['id' => $productFilter->getFilter()->getId()]);
                        $filterName = $filter->getName();
                        $filterId = $filter->getSlug();

                        if (isset($filters[$filterName][$filterId]) && $filterName !== 'Модель') {

                            if (!in_array($productFilter->getFilter()->getValues()[$productFilter->getValue()], $filters[$filterName][$filterId])) {
                                if ($productFilter->getFilter()->getValues()[$productFilter->getValue()]) {
                                    $filters[$filterName][$filterId][] = $productFilter->getFilter()->getValues()[$productFilter->getValue()];
                                }
                            }

                        } else {
                            $filters[$filterName]['sort_order'] = $productFilter->getFilter()->getSortOrder();
                            if ($filterName !== 'Модель') {
                                if (isset($filters[$filterName][$filterId])) {
                                    $filters[$filterName][$filterId] = $this->getFilter($productFilter, $filters[$filterName][$filterId]);
                                } else {
                                    $filters[$filterName][$filterId] = $this->getFilter($productFilter, []);
                                }
                            } else {

                                if (isset($filters[$filterName][$filterId])) {
                                    $filters[$filterName][$filterId] = $this->getModelFilter($productFilter, $filters[$filterName][$filterId]);

                                } else {
                                    $filters[$filterName][$filterId] = $this->getModelFilter($productFilter, []);
                                }


                            }
                        }
                    }

                    sort($filters[$filterName][$filterId], SORT_REGULAR);
                }
            }


        }


        return $filters;
    }


    public function getFilter($productFilter, $filters)
    {

        $productFilters = $productFilter->getFilter()->getValues();
        $currentProductValue = $productFilter->getValue();
        if ($productFilter->getValue() > -1) {
            if (!in_array($productFilters[$currentProductValue], $filters)) {
                $filters[] = $productFilters[$currentProductValue];
            }
        } else {
            if (!empty($productFilters)) {
                $filters = $productFilter->getFilter()->getValues();
            }
        }


        return $filters;
    }


    public function getModelFilter($productFilter, $filters)
    {

        $modelFilterValueId = array_keys($productFilter->getAdditive())[0];

        $additiveFilter = $this->filterValuesRepository->findOneBy(['id' => $modelFilterValueId]);

        $additiveFilterName = $additiveFilter->getName();

        if (!in_array($additiveFilterName, $filters)) {
            $filters[] = $additiveFilterName;
        }


        return $filters;
    }


    public function getCategoriesAndProductIds($children, $category)
    {
        $ids = [];
        $categories = [];
        $filters = [];

        if (count($children) > 0) {

            foreach ($children as $child) {
                if ($child->getProducts()->count() > 0) {

                    $products = $child->getProducts()->getValues();

                    foreach ($products as $product) {
                        $ids[] = $product->getId();

                        if ($product->getProductFilters()->count() > 0 && $category->getLvl() > 0) {

                            $productFilters = $product->getProductFilters()->getValues();

                            foreach ($productFilters as $productFilter) {
                                if ($product->getStatus()->getId() === ProductStatuses::STATUS_ACTIVE) {
                                    $filter = $this->filterRepository->findOneBy(['id' => $productFilter->getFilter()->getId()]);
                                    $filterName = $filter->getName();
                                    $filterId = $filter->getSlug();

                                    if (isset($filters[$filterName][$filterId]) && $filterName !== 'Модель') {

                                        if (!in_array($productFilter->getFilter()->getValues()[$productFilter->getValue()], $filters[$filterName][$filterId])) {
                                            if ($productFilter->getFilter()->getValues()[$productFilter->getValue()]) {
                                                $filters[$filterName][$filterId][] = $productFilter->getFilter()->getValues()[$productFilter->getValue()];
                                            }
                                        }

                                    } else {
                                        $filters[$filterName]['sort_order'] = $productFilter->getFilter()->getSortOrder();
                                        if ($filterName !== 'Модель') {
                                            if (isset($filters[$filterName][$filterId])) {
                                                $filters[$filterName][$filterId] = $this->getFilter($productFilter, $filters[$filterName][$filterId]);
                                            } else {
                                                $filters[$filterName][$filterId] = $this->getFilter($productFilter, []);
                                            }
                                        } else {

                                            if (isset($filters[$filterName][$filterId])) {
                                                $filters[$filterName][$filterId] = $this->getModelFilter($productFilter, $filters[$filterName][$filterId]);

                                            } else {
                                                $filters[$filterName][$filterId] = $this->getModelFilter($productFilter, []);
                                            }


                                        }


                                    }

                                    sort($filters[$filterName][$filterId], SORT_REGULAR);
                                }
                            }




                        }
                        if ($product->getStatus()->getId() === ProductStatuses::STATUS_ACTIVE) {
                            if ($product->getCategory()->getLvl() > $category->getLvl()) {
                                $categories[] = $this->categoryRepository->getPath($product->getCategory())[$category->getLvl() + 1];
                            } else {
                                $categories[] = $product->getCategory();

                            }
                        }
                    }


                }


            }
        }


        $categories = array_unique($categories, SORT_REGULAR);

        return [$ids, $categories, $filters];
    }

    public function getProductsFromCategory(Category $category, $query, $includeNode = true)
    {
        $children = $this->categoryRepository->getChildrenQueryBuilder($category, false, 'sort_order', 'ASC', $includeNode)
            ->andWhere('node.status = :status')->setParameter('status', CategoryStatuses::STATUS_ACTIVE)->getQuery()->getResult();

        $result = [];
        list($productIds, $categories, $filters) = $this->getCategoriesAndProductIds($children, $category);


        if (!empty($productIds)) {
            $result = $query->setParameter('id', $productIds);
        }


        return [$result, $categories, $filters];

    }


    public function getProductTypes(Category $category)
    {
        $children = $this->categoryRepository->getChildren($category, false, 'sort_order', 'ASC', true);
        list($productIds, $categories, $filters) = $this->getCategoriesAndProductIds($children, $category);

        $query = $this->productRepository->getProductsByIdQuery();

        $result = [];
        if (!empty($productIds)) {
            $result = $query->setParameter('id', $productIds)
                ->leftJoin('p.type', 'pt')
                ->select('distinct pt.name, pt.slug')
                ->getQuery()
                ->getResult();

        }
        return $result;
    }


    public function deleteCategoryTree(Category $category, $parentCategory)
    {
        $children = $category->getChildren()->getValues();

        if (count($children) > 0) {
            foreach ($children as $child) {
                $this->updateProductCategory($child->getId(), $parentCategory);
                $this->entityManager->remove($child);
                $this->deleteCategoryTree($child, $parentCategory);
            }
        } else {
            $this->updateProductCategory($category->getId(), $parentCategory);
        }

    }


    public function deleteCategories(Category $category, $parentCategory)
    {
        $this->deleteCategoryTree($category, $parentCategory);
        $this->entityManager->remove($category);
        $this->entityManager->flush();
    }


    public function getParentFilters($category, $action = 'new', $selectedFilters = null)
    {
        $path = $this->categoryRepository->getPath($category);
        $parentCategories = array_slice($path, 1);
        $categoryIndex = array_search($category, $path);

        $filters = [];
        $filter = [];
        while ($categoryIndex > 0) {

            if ($path[$categoryIndex]->getFilters()->count() > 0) {
                $filter = $path[$categoryIndex]->getFilters()->getValues();
                if ($action === 'new') {
                    foreach ($filter as $item) {
                        if ($item->getType() > 0) {
                            $item->setValues([]);
                        }
                    }
                } else {

                    foreach ($filter as $item) {
                        foreach ($selectedFilters as $selectedFilter) {

                            if ($selectedFilter->getFilter() === $item) {

                                if ($selectedFilter->getValue() === -1) {
                                    $item->setValues([]);
                                } else {
                                    if ($item->getType() > 0) {
                                        $item->setValues([$item->getValues()[$selectedFilter->getValue()]]);
                                    }
                                }
                            }
                        }

                    }
                }
            }
            $categoryIndex--;
            $filters = array_merge($filters, $filter);
        }


        return $this->sortFilters($filters);
    }

    public function sortFilters($filters)
    {
        usort($filters, function ($left, $right) {
            if (is_null($left->getSortOrder())) return 1;
            if (is_null($right->getSortOrder())) return -1;
            return $left->getSortOrder() > $right->getSortOrder();
        });

        return $filters;
    }


    public function getEditCategoriesData(Product $product)
    {
        $selectedFilters = $product->getProductFilters()->getValues();
        $tree = $this->categoryRepository->getPath($product->getCategory());
        $data = [];


        $data['choices'][] = $this->categoryRepository->findCategoriesByLvl(0);
        foreach ($tree as $key => $leaf) {
            $values = $leaf->getChildren()->getValues();
            if (!empty($values) && count($tree) > 1) {
                $data['choices'][] = $this->collectionNormalizer->normalize($values);
            }
            if (!$leaf->getFilters()->isEmpty()) {

                foreach ($selectedFilters as $selectedFilter) {

                    if ($leaf->getFilters()->contains($selectedFilter->getFilter())) {

                        if ($selectedFilter->getFilter()->getId() === 3) {
                            $productValues = $product->getProductFilters()->getValues();
                            foreach ($productValues as $productValue) {
                                if (!empty($productValue->getAdditive())) {
                                    $selectedModelValue = array_values($productValue->getAdditive())[0];
                                    $data['selectedFilters'][] = ['id' => $selectedFilter->getFilter()->getId(), 'value' => $selectedModelValue];
                                }
                            }
                        } elseif ($selectedFilter->getFilter()->getId() === 2) {
                            $data['models'] = $this->collectionNormalizer->normalize($this->filterValuesRepository->findBy(['value_id' => $selectedFilter->getValue()]));
                            $data['selectedFilters'][] = ['id' => $selectedFilter->getFilter()->getId(), 'value' => $selectedFilter->getValue()];
                        } else {
                            if ($selectedFilter->getFilter()->getType() > 0) {
                                $filter = $this->filterRepository->findOneBy(['id' => $selectedFilter->getFilter()->getId()]);
                                $valueIndex = $selectedFilter->getValue();

                                if ($valueIndex > -1) {
                                    $data['selectedFilters'][] = ['id' => $selectedFilter->getFilter()->getId(), 'value' => $filter->getValues()[$valueIndex], 'type' => $selectedFilter->getFilter()->getType()];
                                }
                            } else {
                                $data['selectedFilters'][] = ['id' => $selectedFilter->getFilter()->getId(), 'value' => $selectedFilter->getValue(), 'type' => $selectedFilter->getFilter()->getType()];
                            }
                        }
                    }

                }

                $data['filters'] = $this->filterNormalizer->normalize($this->getParentFilters($product->getCategory(), 'edit', $selectedFilters));

            }

            if ($leaf->getLvl() < 1) {
                $data['types'] = $this->collectionNormalizer->normalize($leaf->getProductTypes()->getValues());
            }
            $data['lvl'] = $key;
            $data['selectedCategories'][] = $leaf->getId();
        }


        return $data;
    }

}