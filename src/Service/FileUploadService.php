<?php


namespace App\Service;

use Symfony\Component\Filesystem\Exception\IOException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\Exception\FileException;



class FileUploadService
{

    /**
     * @var Filesystem
     */
    private $filesystem;


    public function __construct(
        Filesystem $filesystem
    )
    {
        $this->filesystem = $filesystem;
    }

    public function deleteFile($fileName) {
        if ($this->filesystem->exists($fileName)) {
            $this->filesystem->remove($fileName);
        }  else {
            throw new IOException('File not found');
        }
    }

    public function createDir($fileUserDir){
        if (!$this->filesystem->exists( $fileUserDir)) {
            $this->filesystem->mkdir( $fileUserDir, 0755);
        }
    }

    public function moveFile($imageFile, $newFilename, $path) {
        try {

            $imageFile->move(
                $path,
                $newFilename
            );
        } catch (FileException $e) {
            throw new FileException('File was not uploaded successfully');
        }
    }


}