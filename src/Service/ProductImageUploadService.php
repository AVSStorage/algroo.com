<?php


namespace App\Service;


use App\Entity\Product;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use Intervention\Image\ImageManager;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Filesystem\Exception\IOException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\String\Slugger\SluggerInterface;

class ProductImageUploadService
{
    /**
     * @var FileUploadService
     */
    private $fileUploadService;
    /**
     * @var Security
     */
    private $security;
    /**
     * @var ProductService
     */
    private $productService;
    /**
     * @var ParameterBagInterface
     */
    private $parameters;
    /**
     * @var SluggerInterface
     */
    private $slugger;
    /**
     * @var Filesystem
     */
    private $filesystem;
    /**
     * @var ProductRepository
     */
    private $productRepository;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * ProductImageUploadService constructor.
     * @param Filesystem $filesystem
     * @param SluggerInterface $slugger
     * @param ParameterBagInterface $parameters
     * @param Security $security
     * @param ProductService $productService
     * @param FileUploadService $fileUploadService
     */
    public function __construct(
        Filesystem $filesystem,
        SluggerInterface $slugger,
        ParameterBagInterface $parameters,
        Security $security,
        ProductService $productService,
        FileUploadService $fileUploadService,
        ProductRepository $productRepository,
    EntityManagerInterface $entityManager

    )
    {
        $this->filesystem = $filesystem;
        $this->slugger = $slugger;
        $this->parameters = $parameters;
        $this->security = $security;
        $this->productService = $productService;
        $this->fileUploadService = $fileUploadService;
        $this->productRepository = $productRepository;
        $this->entityManager = $entityManager;
    }



    public function updateImageArray(Product $product, $imageName) {
        $photos = $product->getPhotos();
        $index  = array_search($imageName, $photos);
        if ($index) {
            unset($photos[$index]);
        }
        $product->setPhotos($photos);
        $this->entityManager->persist($product);
        $this->entityManager->flush();
    }

    public function deleteProductPhoto($imageName, $action, $productId = null) {

        if ($action === 'edit' && $productId) {
            $image = $this->productService->getSaveImageFolder($productId) . '/' . $imageName;
            $thumbnail = $this->productService->getSaveImageFolder($productId) . '/thumbnails/' . $imageName;



            if ($this->filesystem->exists($image)) {
                $product = $this->productRepository->findOneBy(['id' => $productId]);
                $this->updateImageArray($product, $imageName);
                $this->fileUploadService->deleteFile($image);
                $this->fileUploadService->deleteFile($thumbnail);
            }


            /**
             *   If edit mode image is deleted
             */

            $image = $this->productService->getProductImageFolder($productId) . '/' . $imageName;
            $thumbnail = $this->productService->getProductImageFolder($productId) . '/thumbs/' . $imageName;

            if ($this->filesystem->exists($image)) {
                $this->fileUploadService->deleteFile($image);
                $this->fileUploadService->deleteFile($thumbnail);
            }

        } else {
            $image = $this->productService->getProductTempImageFolder() . '/' . $imageName;
            $thumbnail = $this->productService->getProductTempThumbnailsFolder() . '/' . $imageName;

            $this->fileUploadService->deleteFile($image);
            $this->fileUploadService->deleteFile($thumbnail);
        }


    }


    public function fitImage($path, $savepath, $imageSize) {
        $imgDriver  = new ImageManager(array('driver' => 'gd'));
        $img = $imgDriver->make($path);
        $img->fit($imageSize, $imageSize);
        $img->save($savepath);
    }




    public function setProductPhoto(UploadedFile $imageFile, $productId)
    {


        $originalFilename = pathinfo($imageFile->getClientOriginalName(), PATHINFO_FILENAME);
        // this is needed to safely include the file name as part of the URL
        $safeFilename = $this->slugger->slug($originalFilename);
        $prefix = $safeFilename . '-' . uniqid();
        $newFilename = $prefix . '.' . $imageFile->guessExtension();

        if ($productId) {

            $productDir = $this->productService->getProductImageFolder($productId);
            $fileThumbDir = $this->productService->getProductImageFolder($productId) .'/thumbs';
            if(!$this->filesystem->exists($fileThumbDir)){
                $this->fileUploadService->createDir($fileThumbDir);
            }


        } else {

            $productDir = $this->productService->getProductTempImageFolder();
            $fileThumbDir = $this->productService->getProductTempThumbnailsFolder();


            $this->fileUploadService->createDir($productDir);
            $this->fileUploadService->createDir($fileThumbDir);

        }



        $this->fitImage($imageFile->getPathname(),
            $imageFile->getPathname(),
            578);

        $this->fileUploadService->moveFile($imageFile, $newFilename,  $productDir);


        $this->fitImage($productDir . '/' . $newFilename,
            $fileThumbDir . '/' . $newFilename ,107);



        return [ 'full_path' => $fileThumbDir . '/' . $newFilename, 'image' => $newFilename];
    }
}