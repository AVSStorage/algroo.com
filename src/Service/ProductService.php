<?php


namespace App\Service;


use App\Entity\Category;
use App\Entity\CategoryStatuses;
use App\Entity\Product;
use App\Entity\ProductFilter;
use App\Entity\ProductStatuses;
use App\Entity\User;
use App\Event\ModeratedProductEvent;
use App\Event\NewProductEvent;
use App\Event\NotModeratedProductEvent;
use App\EventSubscriber\ProductSubscriber;
use App\Repository\CategoryRepository;
use App\Repository\CategoryStatusesRepository;
use App\Repository\FilterRepository;
use App\Repository\FilterValuesRepository;
use App\Repository\ProductFilterRepository;
use App\Repository\ProductRepository;
use App\Repository\ProductStatusesRepository;
use App\Repository\ProductTypeRepository;
use App\Repository\UserRepository;
use App\Security\UserAuthenticator;
use Doctrine\ORM\EntityManagerInterface;
use Psr\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\String\Slugger\SluggerInterface;

class ProductService
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var ProductRepository
     */
    private $productRepository;
    /**
     * @var Filesystem
     */
    private $filesystem;
    /**
     * @var SluggerInterface
     */
    private $slugger;
    /**
     * @var UserPasswordEncoderInterface
     */
    private $userPasswordEncoder;
    /**
     * @var ProductTypeRepository
     */
    private $productTypeRepository;
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var FilterRepository
     */
    private $filterRepository;
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;
    /**
     * @var ParameterBagInterface
     */
    private $parameters;


    /**
     * @var Security
     */
    private $security;


    const IMAGE_TEMP_DIR = '/temp/';

    const PRODUCT_IMAGES_COUNT = 5;
    /**
     * @var FileUploadService
     */
    private $fileUploadService;
    /**
     * @var CategoryStatusesRepository
     */
    private $categoryStatusesRepository;
    /**
     * @var ProductStatusesRepository
     */
    private $productStatusesRepository;
    /**
     * @var EventDispatcher
     */
    private $eventDispatcher;
    /**
     * @var ProductFilterRepository
     */
    private $productFilterRepository;
    /**
     * @var FilterValuesRepository
     */
    private $filterValuesRepository;
    /**
     * @var \Psr\Container\ContainerInterface
     */
    private $container;
    /**
     * @var ProductSubscriber
     */
    private $productSubscriber;

    /**
     * ProductService constructor.
     * @param EntityManagerInterface $entityManager
     * @param ProductRepository $productRepository
     * @param Filesystem $filesystem
     * @param SluggerInterface $slugger
     * @param UserPasswordEncoderInterface $userPasswordEncoder
     * @param ProductTypeRepository $productTypeRepository
     * @param UserRepository $userRepository
     * @param FilterRepository $filterRepository
     * @param CategoryRepository $categoryRepository
     * @param ParameterBagInterface $parameters
     * @param FileUploadService $fileUploadService
     * @param Security $security
     * @param CategoryStatusesRepository $categoryStatusesRepository
     * @param ProductStatusesRepository $productStatusesRepository
     * @param EventDispatcher $eventDispatcher
     */
    public function __construct(EntityManagerInterface $entityManager,
                                ProductRepository $productRepository,
                                Filesystem $filesystem,
                                SluggerInterface $slugger,
                                UserPasswordEncoderInterface $userPasswordEncoder,
                                ProductTypeRepository $productTypeRepository,
                                UserRepository $userRepository,
                                FilterRepository $filterRepository,
                                CategoryRepository $categoryRepository,
                                ParameterBagInterface $parameters,
                                FileUploadService $fileUploadService,
                                Security $security,
                                CategoryStatusesRepository $categoryStatusesRepository,
                                ProductStatusesRepository $productStatusesRepository,
                                EventDispatcherInterface $eventDispatcher,
    \Psr\Container\ContainerInterface $container,
    ProductSubscriber $productSubscriber,
    ProductFilterRepository $productFilterRepository,
    FilterValuesRepository $filterValuesRepository
    )
    {
        $this->entityManager = $entityManager;
        $this->productRepository = $productRepository;
        $this->filesystem = $filesystem;
        $this->slugger = $slugger;
        $this->userPasswordEncoder = $userPasswordEncoder;
        $this->productTypeRepository = $productTypeRepository;
        $this->userRepository = $userRepository;
        $this->filterRepository = $filterRepository;
        $this->categoryRepository = $categoryRepository;
        $this->parameters = $parameters;

        $this->security = $security;
        try {
            $this->imageDir = $this->parameters->get('image_directory') . $this->security->getUser()->id;
            $this->fileTempUserDir = $this->parameters->get('image_directory') . $this->security->getUser()->id . $this->parameters->get('image_temp_directory');
            $this->fileUserDir = $this->parameters->get('image_directory') . $this->security->getUser()->id;
        } catch (\ErrorException $exception) {

        }
        $this->fileUploadService = $fileUploadService;
        $this->categoryStatusesRepository = $categoryStatusesRepository;
        $this->productStatusesRepository = $productStatusesRepository;
        $this->eventDispatcher = $eventDispatcher;

        $this->filterValuesRepository = $filterValuesRepository;
        $this->productFilterRepository = $productFilterRepository;
        $this->container = $container;
        $this->productSubscriber = $productSubscriber;
    }

    public function setProductFilters($filters, Product $product)
    {



        foreach ($filters as $filterKey => $filterValue) {

            if ($filterValue) {


                $productFilter = $this->productFilterRepository->findOneBy(['filter' => $filterKey, 'product' => $product->getId()]);

                if (!$productFilter) {
                    $productFilter = new ProductFilter();
                    if ($this->filterRepository->findOneBy(['id' => $filterKey])) {
                        $productFilter->setFilter($this->filterRepository->findOneBy(['id' => $filterKey]));
                    }
                }

                // Если текстовый фильтр
                if (!isset($filterValue[1])) {
                    $productFilter->setValue((int)$filterValue[0]);
                } else {
                    $textFilter = $this->filterRepository->findOneBy(['id' => $filterKey]);
                    $values = $textFilter->getValues();


                        if (!in_array($filterValue[1], $values)) {
                            if ($filterValue[1] !== '') {
                                array_push($filterValue[1],$values);
                                $textFilter->setValues($values);
                                $values = $textFilter->getValues();

                                $productFilter->setValue(array_search($filterValue[1], $values));
                            } else {
                                $productFilter->setValue(-1);
                            }


                            $this->entityManager->persist($textFilter);

                        } else {
                            $productFilter->setValue(array_search($filterValue[1], $values));
                        }




                }



                    $product->addProductFilter($productFilter);
                    $this->entityManager->persist($productFilter);

            }

        }
    }


    public function createImageDir()
    {
        $fileUserDir = $this->parameters->get('image_directory') . $this->security->getUser()->id;
        $this->fileUploadService->createDir($fileUserDir);
    }

    public function getCurrentFiles($find, $productId, $fileNames)
    {
        if (in_array(User::ROLE_ADMIN, $this->security->getUser()->getRoles())) {
            $product = $this->productRepository->findOneBy(['id' => $productId ]);
            $dirName = $this->parameters->get('image_directory') . $product->getUser()->id;
            $files = $find->files()->in( $dirName . '/' . $productId);
        } else {
            $files = $find->files()->in($this->imageDir . '/' . $productId);
        }
        foreach ($files as $file) {
            if (!strpos($file->getPath(), 'thumbnails')) {
                $fileNames[] = $file->getFileName();
            }
        }

        return $fileNames;
    }

    public function setProductPhotos($action, $productId)
    {

        $find = new Finder();
        $fileNames = [];
        if ($action === 'edit' && $productId) {

            $fileUserDir = $this->imageDir . '/' . $productId;
            if ($this->filesystem->exists($this->imageDir . '/temp/' . $productId)) {

                $productImages = $find->files()->in($this->imageDir . '/temp/' . $productId);


                if ($productImages->count() > 0) {
                    /**
                     *  Photos updated in edit mode
                     */
                    $fileNames = $this->saveProductImage($productImages, $fileUserDir);
                    $fileNames = $this->getCurrentFiles($find, $productId, $fileNames);
                    $fileNames = array_unique($fileNames);
                } else {
                    /**
                     *  Photos not updated in edit mode
                     */
                    $fileNames = $this->getCurrentFiles($find, $productId, $fileNames);
                }

            } else {
                /**
                 *  Photos not updated in edit mode
                 */
                $fileNames = $this->getCurrentFiles($find, $productId, $fileNames);
            }


        } else {


            $fileUserDir = $this->imageDir . '/' . $this->getProductAddedId();

            $this->fileUploadService->createDir($fileUserDir);
            $this->fileUploadService->createDir($fileUserDir . '/thumbnails');

            $productImages = $find->files()->in($this->getProductTempImageFolder());

            if ($productImages->count() < 1) {
                throw new FileNotFoundException('Product Images not loaded');
            }

            $fileNames = $this->saveProductImage($productImages, $fileUserDir);
        }


        return $fileNames;
    }


    public function getEditPhotosArray($imagePath)
    {
        $finder = new Finder();
        $images = $finder->files()->in($imagePath);


        $imageData = [];
        $counter = 0;
        foreach ($images as $image) {
            $counter++;
            $imageData[$counter - 1]['id'] = $counter;
            $imageData[$counter - 1]['original'] = $image->getFileName();
            $imageData[$counter - 1]['uploaded'] = true;
            $imageData[$counter - 1]['action'] = 'edit';
            $imageData[$counter - 1]['value'] = $image->getPath() . '/' . $image->getFileName();
        }


        if (count($imageData) < self::PRODUCT_IMAGES_COUNT) {
            $index = count($imageData);

            while ($index < self::PRODUCT_IMAGES_COUNT) {

                $imageData[$index]['id'] = $index + 1;
                $imageData[$index]['original'] = '';
                $imageData[$index]['uploaded'] = false;
                $imageData[$index]['action'] = 'edit';
                $imageData[$index]['value'] = '';
                $index++;
            }
        }


        return $imageData;
    }


    public function saveProductImage($productImages, $savePath)
    {

        $fileNames = [];
        foreach ($productImages as $productImage) {


            $imageFile = new UploadedFile($productImage->getRealPath(), $productImage->getFilename(), null, null, true);


            if (strpos($productImage->getPath(), 'thumbs')) {
                $this->fileUploadService->moveFile($imageFile, $productImage->getFilename(), $savePath . '/thumbnails');
            } else {
                $this->fileUploadService->moveFile($imageFile, $productImage->getFilename(), $savePath);
                $fileNames[] = $productImage->getFileName();
            }


        }

        return $fileNames;
    }

    public function getProductAddedId()
    {
        if ($this->parameters->get('current_db') !== 'postgres') {
            $conn = $this->entityManager
                ->getConnection();
            $sql = "SELECT `AUTO_INCREMENT`
FROM  INFORMATION_SCHEMA.TABLES
WHERE TABLE_SCHEMA = '" . $this->parameters->get('database_name') . "'
AND   TABLE_NAME   = 'product'";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $index = $stmt->fetchColumn();
            return $index;
        } else {
             return $this->productRepository->findOneBy([], ['id' => 'DESC'])->getId() + 1;
        }
    }

    public function getProductTempImageFolder()
    {
        return $this->fileTempUserDir . '/' . $this->getProductAddedId();
    }

    public function getProductImageFolder($id)
    {
        return $this->fileTempUserDir . '/' . $id;
    }



    public function clearTempDir()
    {
        $finder = new Finder();
        if ($this->filesystem->exists($this->getTempProductImageDir())) {
            $tempDir = $finder->directories()->in($this->getTempProductImageDir());


            if ($tempDir->count() > 0) {
                foreach ($tempDir as $dir) {
                    $this->fileUploadService->deleteFile($dir);
                    break;
                }

                $this->clearTempDir();
            }
        }
    }

    public function getTempProductImageDir()
    {
        return $this->fileTempUserDir;
    }

    public function getSaveImageFolder($id)
    {
        return $this->imageDir . '/' . $id;
    }

    public function getProductTempThumbnailsFolder()
    {
        return $this->getProductTempImageFolder() . '/thumbs';
    }

    public function notifyModerated($product) {

        $url = $this->container->get('router')->generate('product',['id' =>  $product->getId()], UrlGeneratorInterface::ABSOLUTE_URL);

        // dispatch
        $this->eventDispatcher->dispatch( new ModeratedProductEvent($product, $url), ModeratedProductEvent::NAME);
    }

    public  function notifyNotModerated($product, $reason) {

        $url = $this->container->get('router')->generate('dashboard_products', [], UrlGeneratorInterface::ABSOLUTE_URL);
        // dispatch
        $this->eventDispatcher->dispatch( new NotModeratedProductEvent($product, $url, $reason), NotModeratedProductEvent::NAME);

    }
    public function setNewProduct(FormInterface $form, Product $product, $action = 'new',$filters, $newCategory = null, $models = null)
    {


        $isAdmin = in_array(User::ROLE_ADMIN, $this->security->getUser()->getRoles());
        if (!in_array('', array_values($filters))) {
            $this->setProductFilters($filters, $product);
        }




        if ((int)$form->get('category')->getData() === 136) {
            $array = [];



            if ($models) {
                $filter = $this->filterRepository->findOneBy(['id' => 3]);
                $productFilter = new ProductFilter();

                $filterValue = $this->filterValuesRepository->findBy(['value_id' => $filters[2]])[$models];
                $array[$filterValue->getId()] = $models;

                $productFilter->setAdditive($array);
                $productFilter->setValue(0);
                $productFilter->setFilter($filter);
                $productFilter->setProduct($product);

                $this->entityManager->persist($productFilter);

            } else {
                $existModel = $this->productFilterRepository->findOneBy(['product' => $product->getId(), 'filter' => 3]);
                if ($existModel) {
                    $this->entityManager->remove($existModel);
                }
            }
        }



        if ($newCategory) {
            $category = new Category();
            $category->setName($newCategory['name']);
            $category->setParent($this->categoryRepository->findOneBy(['id' => $newCategory['parent']]));
            $category->setStatus($this->categoryStatusesRepository->findOneBy(['id' => CategoryStatuses::STATUS_MODERATION]));
            $this->entityManager->persist($category);
        } else {
            $category = $this->categoryRepository->findOneBy(['id' => $form->get('category')->getData()]);
        }



        if ($action === 'edit' && $isAdmin){
            $status = $form->get('status')->getData();

            if ($status) {
                $product->setStatus($status);
                if ((int)$status->getId() !== ProductStatuses::STATUS_NOT_MODERATED ) {
                    $product->setReason(null);

                } else {

                    $product->setReason($form->get('reason')->getData());

                }

                if ((int)$status->getId() === ProductStatuses::STATUS_NOT_MODERATED) {
                    $this->notifyNotModerated($product, $form->get('reason')->getData());
                }

                if ((int)$status->getId() === ProductStatuses::STATUS_ACTIVE) {
                    $this->notifyModerated($product);
                }
            }
        } elseif ($action !== 'edit') {
            $product->setStatus($this->productStatusesRepository->findOneBy(['id' => ProductStatuses::STATUS_NEW]));
        }

        $product->setCategory($category);
        $product->addType($this->productTypeRepository->findOneBy(['id' => $form->get('type')->getData()]));

        if ($isAdmin) {
            $product->setUser($this->userRepository->findOneBy(['id' => $product->getUser()->id]));
        } else {
            $product->setUser($this->userRepository->findOneBy(['id' => $this->security->getUser()->id]));
        }



        $fileNames = [];


        try {
            $fileNames = $this->setProductPhotos($action, $product->getId());
        } catch (FileNotFoundException $exception) {
            $form->addError(new FormError('Изображения не были загружены'));
        }



        $product->setPhotos($fileNames);


        $this->entityManager->persist($product);
        $this->entityManager->flush();
    }
}