<?php


namespace App\Controller\API;


use App\Entity\Chat;
use App\Entity\Message;
use App\Repository\ChatRepository;
use App\Repository\MessageRepository;
use App\Repository\ProductRepository;
use App\Repository\UserLegalRepository;
use App\Repository\UserRepository;
use App\Serializer\Normalizer\MessageNormalizer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;


class ChatAPIController extends  AbstractController
{
    private $normilizer;
    public function __construct()
    {
        $this->normilizer = new MessageNormalizer(new ObjectNormalizer());
    }

    /**
     * @Route("/api/conversations/{user}", name="chat_message_send", format="json", methods={"POST"}, condition="request.isXmlHttpRequest()")
     */
    public function saveMessage(Request $request, UserRepository $userRepository, ChatRepository $chatRepository, MessageRepository $messageRepository, ProductRepository $productRepository, EntityManagerInterface $entityManager)
    {
        $productId = $request->request->get('product');
        $content = $request->request->get('content');

        $channelId = $request->request->get('channel');
        $from_user = $request->request->get('from_user');
        $to_user = $request->request->get('to_user');
        $product = $productRepository->findOneBy(['id' => $productId]);
        $chat = $chatRepository->findOneBy(['slug' => $channelId]);
        if (!$chat) {

            $chat = new Chat();
            $chat->setSlug($channelId);
            $chat->setFromUser($userRepository->findOneBy(['id' => $from_user]));
            $chat->setToUser($userRepository->findOneBy(['id' => $to_user]));
            $chat->setProduct($product);
            $entityManager->persist($chat);
            $entityManager->flush();
        }

        if ($chat->getToUser()->getId() === $this->getUser()->getId()) {
            $to_user = $chat->getFromUser()->getId();
        } else {
            $to_user = $chat->getToUser()->getId();
        }
        if ($content) {

            $message = new Message();
            $message->setAuthor($userRepository->findOneBy(['id' => $from_user]));
            $message->addChat($chat);
            $message->setContent($content);
            $entityManager->persist($message);
            $entityManager->flush();

            return new JsonResponse([
                'message' => [
                'id' => $message->getId(),
                'text' => $content,
                'is_read' => false,
                'to_user' => $to_user,
                'from_user' => $this->getUser()->getId()
                    ]
            ], 200);
        }
    }

    /**
     * @Route("/api/conversations/{channelId}", name="chat_message_get", methods={"GET"}, condition="request.isXmlHttpRequest()")
     */
    public function getMessages(Request $request,  MessageRepository $messageRepository, ChatRepository $chatRepository)
    {


        $chatId = $request->attributes->get('channelId');
        $chat = $chatRepository->findOneBy(['slug' => $chatId]);


        if ($chat->getToUser()->getId() === $this->getUser()->getId()) {
            $to_user = $chat->getToUser()->getId();
            } else {
            $to_user = $chat->getFromUser()->getId();

        }



        return new JsonResponse($this->normilizer->normalize($chat->getMessages()->getValues(), null,['to_user' => $to_user] ), 200);
    }

    /**
     * @Route("/api/messages/read/{messageId}", name="chat_message_set_read", methods={"POST"}, condition="request.isXmlHttpRequest()")
     */
    public function markMessageAsRead(Request $request,  MessageRepository $messageRepository, EntityManagerInterface $entityManager)
    {

        $message = $messageRepository->findOneBy(['id' => $request->attributes->get('messageId')]);
        if ($message) {
            $datetime = new \DateTime('now');
            $timezone = new \DateTimeZone('Europe/Moscow');
            $datetime->setTimezone($timezone);
            $message->setReadAt($datetime);
            $entityManager->persist($message);
            $entityManager->flush();
            return new JsonResponse(['message' => $message], Response::HTTP_OK);
        }

        return new JsonResponse(['error' => 'Такого ID не существует'], Response::HTTP_NOT_FOUND);


    }


}