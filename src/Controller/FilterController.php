<?php

namespace App\Controller;

use App\Entity\Filter;
use App\Form\FilterFormType;
use App\Repository\CategoryRepository;
use App\Repository\FilterRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class FilterController extends AbstractController
{
    /**
     * @Route("/filter/{id}/delete", name="filter_delete")
     * @IsGranted("ROLE_ADMIN")
     */
    public function delete(Request $request, FilterRepository $filterRepository, EntityManagerInterface $entityManager)
    {
        $id = $request->attributes->get('id');
        $filter = $filterRepository->findOneBy(['id' => $id]);

        if ($filter) {
            $entityManager->remove($filter);
            $entityManager->flush();
        }
        return $this->redirectToRoute('admin_filters');
    }

    /**
     * @Route("admin/filter/add/text", name="filter_add_text")
     * @IsGranted("ROLE_ADMIN")
     */
    public function addTextFilter(Request $request, CategoryRepository $categoryRepository, EntityManagerInterface $entityManager) {
        $filter = new Filter();
        $form = $this->createForm(FilterFormType::class, $filter, ['has_category' => true,'text_filter' => true]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){

            $category = $categoryRepository->findOneBy(['id' => $form->get('category')->getData()]);
            $name = $form->get('name')->getData();
            $filter->setName($name);
            $filter->setType(Filter::TEXT_FILTER);
            $filter->addCategory($category);
            $entityManager->persist($filter);
            $entityManager->flush();
            $this->addFlash('success','Фильтр был успешно создан');
            return $this->redirect($request->getUri());
        }



        $choices = [];
        $i = 0;
        while ($i < 2) {
            $choices[] = $categoryRepository->childrenQueryBuilder()->where('node.lvl = '. $i)->getQuery()->getArrayResult();
            $i++;
        }



        return $this->render('admin/filters-add.html.twig', [
            'form' => $form->createView(),
            'action' => 'add',
            'form_name' => $form->createView()->vars['name'],
            'categoryData' => ['choices' => $choices[0]]
        ]);
    }


    /**
     * @Route("admin/filters/{id}/edit/text", name="filter_edit_text")
     * @IsGranted("ROLE_ADMIN")
     */
    public function editTextFilter(Request $request, FilterRepository $filterRepository, CategoryRepository $categoryRepository, EntityManagerInterface $entityManager) {
        $filter = $filterRepository->findOneBy(['id' => $request->attributes->get('id')]);
        $form = $this->createForm(FilterFormType::class, $filter, ['has_category' => false,'text_filter' => true]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){


            $name = $form->get('name')->getData();
            $filter->setName($name);
            $entityManager->persist($filter);
            $entityManager->flush();
            $this->addFlash('success','Фильтр был успешно обновлен');
            return $this->redirect($request->getUri());
        }


        $filterValues = $filter->getValues();
        $values = [];
        if ($filterValues) {
            foreach ($filterValues as $filterValue) {
                $value['value'] = $filterValue;
                $value['show'] = true;
                $values[] = $value;
            }
        }



        return $this->render('admin/filters/edit.html.twig', [
            'form' => $form->createView(),
            'action' => 'edit',
            'categories' => $filter->getCategory(),
            'values' => $values,
            'form_name' => $form->createView()->vars['name'],
        ]);
    }
}
