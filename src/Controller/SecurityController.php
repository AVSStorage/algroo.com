<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\LoginFormType;
use App\Form\RegistrationLegalFormType;
use App\Repository\AllUsersRepository;
use App\Repository\UserIndividualRepository;
use App\Repository\UserLegalRepository;
use App\Repository\UserRepository;
use App\Serializer\Normalizer\FormNormalizer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="app_login")
     */
    public function login(Request $request, AuthenticationUtils $authenticationUtils, UserIndividualRepository $userIndividualRepository, UserLegalRepository $userLegalRepository, UserRepository $userRepository): Response
    {
        // if ($this->getUser()) {
        //     return $this->redirectToRoute('target_path');
        // }

        $user = new User();
        $form = $this->createForm(LoginFormType::class, $user);
        $form->handleRequest($request);

        $userName = '';


        
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {

            $user = $userRepository->findOneBy(['id' => $this->getUser()->id]);
            $userName = $user->getUserIndividual()->getName();
        }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();



        return $this->render('security/login.html.twig', ['last_username' => $userName, 'error' => $error, 'form' => $form->createView(),]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }
}
