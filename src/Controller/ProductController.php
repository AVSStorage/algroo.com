<?php

namespace App\Controller;

use App\Entity\Filter;
use App\Entity\Product;
use App\Form\ProductFormType;
use App\Repository\AllUsersRepository;
use App\Repository\CategoryRepository;
use App\Repository\ChatRepository;
use App\Repository\FilterRepository;
use App\Repository\MessageRepository;
use App\Repository\ProductRepository;
use App\Repository\ViewCounterRepository;
use App\Serializer\Normalizer\CollectionNormalizer;
use App\Serializer\Normalizer\FilterNormalizer;
use App\Service\CategoryService;
use App\Service\ProductService;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Uid\Uuid;
use Tchoulom\ViewCounterBundle\Counter\ViewCounter;
use Tchoulom\ViewCounterBundle\Finder\StatsFinder;

class ProductController extends AbstractController
{


    /**
     * @Route("/product/{id}", name="product")
     *
     */
    public function index(Request $request, ChatRepository $chatRepository, ViewCounter $viewCounter, MessageRepository $messageRepository, ViewCounterRepository $viewCounterRepository, EntityManagerInterface $entityManager, ProductRepository $repository, AllUsersRepository $allUsersRepository, CategoryRepository $categoryRepository, FilterRepository $filterRepository)
    {
        $product = $repository->findOneBy(['id' => $request->get('id')]);


        $viewcounter = $viewCounter->getViewCounter($product);


        if ($viewCounter->isNewView($viewcounter)) {

            $viewCounter->saveView($product, 0);
        }




        if ($this->getUser()) {
            $chat = $chatRepository->getChatByUserAndProduct($this->getUser()->getId(), $product->getId());
            if ($chat) {
                $slug = $chat[0]->getSlug();
            } else {
                $slug = 'u-' . $this->getUser()->getId() . '-' . Uuid::v4();
            }
        } else {
            $slug =  Uuid::v4();
        }



        return $this->render('product/index.html.twig', [
            'controller_name' => 'ProductController',
            'product' => $product,
            'slug' => $slug,
            'user_info' => $allUsersRepository->findOneBy(['id' => $product->getUser()->getId()]),
            'categories' => $categoryRepository->findBy(['lvl' => 0]),
            'product_category_path' => $categoryRepository->getPath($product->getCategory())
        ]);
    }

    /**
     * @Route("/product/edit/{id}", name="product_edit")
     * @IsGranted("ROLE_USER")
     */
    public function edit(Request $request, ParameterBagInterface $parameterBag, ProductService $productService, CategoryService $categoryService, EntityManagerInterface $entityManager, ProductRepository $productRepository, CategoryRepository $categoryRepository)
    {


        $product = $productRepository->findOneBy(['id' => $request->attributes->get('id')]);
        $form = $this->createForm(ProductFormType::class, $product, ['role' => $this->getUser()->getRoles()]);
        $form->handleRequest($request);
        $data = $categoryService->getEditCategoriesData($product);

        if (!$form->isSubmitted()) {
            // Очистить папку с фотографиями
            $productService->clearTempDir();
            if ($this->isGranted('ROLE_ADMIN')) {
                $form->get('reason')->setData($product->getReason());
            }
        }


        if ($form->isSubmitted() && $form->isValid()) {


            $models = null;

            if ((int)$form->get('category')->getData() === 136) {
                $models = $request->request->get('model');
            }

            $allFormValues = $request->request->get('product_form');
            $filter = $allFormValues['filter'];

            $productService->setNewProduct($form, $product, 'edit', $filter, $newCategory = null, $models);
            return $this->redirect($request->getUri());
        }

        $imagePath = $product->getImageThumbnailPath($parameterBag->get('image_directory'));

        $imageData = $productService->getEditPhotosArray($imagePath);


        return $this->render('dashboard/edit.html.twig', [
            'controller_name' => 'ProductController',
            'form' => $form->createView(),
            'product' => $product,
            'imageData' => $imageData,
            'categoryData' => $data,
            'form_name' => $form->createView()->vars['name']
        ]);
    }

    /**
     * @Route("/product/delete/{id}", name="product_delete")
     * @IsGranted("ROLE_USER")
     */
    public function delete(Request $request, ProductRepository $productRepository, EntityManagerInterface $entityManager, Security $security)
    {

        $product = $productRepository->findOneBy(['id' => $request->attributes->get('id')]);
        $entityManager->remove($product);
        $entityManager->flush();
        if ($security->isGranted('ROLE_ADMIN')) {
            return new RedirectResponse('/admin');
        } else {
            return $this->redirectToRoute('dashboard_products');
        }
    }


    /**
     * @Route("/product/premium/{id}", name="product_premium")
     * @IsGranted("ROLE_USER")
     */
    public function premium(Request $request, ProductRepository $productRepository, EntityManagerInterface $entityManager, Security $security)
    {

        $product = $productRepository->findOneBy(['id' => $request->attributes->get('id')]);
        $product->setIsPremium(true);
        $entityManager->persist($product);
        $entityManager->flush();
        if ($security->isGranted('ROLE_ADMIN')) {

            return new RedirectResponse('/admin');
        } else {
            return $this->redirectToRoute('dashboard_payment');
        }
    }



    /**
     * @Route("/product/premium/del/{id}", name="product_del_premium")
     * @IsGranted("ROLE_USER")
     */
    public function delPremium(Request $request, ProductRepository $productRepository, EntityManagerInterface $entityManager, Security $security)
    {
        $product = $productRepository->findOneBy(['id' => $request->attributes->get('id')]);
        $product->setIsPremium(false);
        $entityManager->persist($product);
        $entityManager->flush();

        if ($security->isGranted('ROLE_ADMIN')) {

            return new RedirectResponse('/admin');
        } else {
            return $this->redirectToRoute('dashboard_payment');
        }
    }

}
