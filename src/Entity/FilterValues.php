<?php

namespace App\Entity;

use App\Repository\FilterValuesRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FilterValuesRepository::class)
 */
class FilterValues
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     */
    private $value_id;

    /**
     * @ORM\ManyToOne(targetEntity=Filter::class, inversedBy="filterValues")
     */
    private $filter;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getValueId(): ?int
    {
        return $this->value_id;
    }

    public function setValueId(int $value_id): self
    {
        $this->value_id = $value_id;

        return $this;
    }

    public function getFilter(): ?Filter
    {
        return $this->filter;
    }

    public function setFilter(?Filter $filter): self
    {
        $this->filter = $filter;

        return $this;
    }
}
