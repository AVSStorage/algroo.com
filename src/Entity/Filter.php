<?php

namespace App\Entity;

use App\Repository\FilterRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass=FilterRepository::class)
 */
class Filter
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;
    

    const SELECT_FILTER = 0;
    const TEXT_FILTER = 1;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity=Category::class, inversedBy="filters")
     */
    private $category;

    /**
     * @ORM\Column(type="array", nullable=true, name="`values`")
     */
    private $values = [];



    /**
     * @ORM\OneToMany(targetEntity=ProductFilter::class, mappedBy="filter", orphanRemoval=true)
     */
    private $productFilters;

    /**
     * @ORM\OneToMany(targetEntity=FilterValues::class, mappedBy="relation")
     */
    private $filterValues;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"default" : 0})
     */
    private $type;

    /**
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(length=128, nullable=true)
     */
    private $slug;

    /**
     * @ORM\Column(type="float", nullable=true, options={"default" : 0})
     */
    private $sort_order;

    public function __construct()
    {
        $this->category = new ArrayCollection();
        $this->products = new ArrayCollection();
        $this->productFilters = new ArrayCollection();
        $this->filterValues = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }


    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Category[]
     */
    public function getCategory(): Collection
    {
        return $this->category;
    }

    public function addCategory(Category $category): self
    {
        if (!$this->category->contains($category)) {
            $this->category[] = $category;
        }

        return $this;
    }

    public function removeCategory(Category $category): self
    {
        if ($this->category->contains($category)) {
            $this->category->removeElement($category);
        }

        return $this;
    }

    public function getValues(): ?array
    {
        return $this->values;
    }

    public function setValues(?array $values): self
    {
        $this->values = $values;

        return $this;
    }

    /**
     * @return Collection|ProductFilter[]
     */
    public function getProductFilters(): Collection
    {
        return $this->productFilters;
    }

    public function addProductFilter(ProductFilter $productFilter): self
    {
        if (!$this->productFilters->contains($productFilter)) {
            $this->productFilters[] = $productFilter;
            $productFilter->setFilter($this);
        }

        return $this;
    }

    public function removeProductFilter(ProductFilter $productFilter): self
    {
        if ($this->productFilters->contains($productFilter)) {
            $this->productFilters->removeElement($productFilter);
            // set the owning side to null (unless already changed)
            if ($productFilter->getFilter() === $this) {
                $productFilter->setFilter(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|FilterValues[]
     */
    public function getFilterValues(): Collection
    {
        return $this->filterValues;
    }

    public function addFilterValue(FilterValues $filterValue): self
    {
        if (!$this->filterValues->contains($filterValue)) {
            $this->filterValues[] = $filterValue;
            $filterValue->setFilter($this);
        }

        return $this;
    }

    public function removeFilterValue(FilterValues $filterValue): self
    {
        if ($this->filterValues->contains($filterValue)) {
            $this->filterValues->removeElement($filterValue);
            // set the owning side to null (unless already changed)
            if ($filterValue->getFilter() === $this) {
                $filterValue->setFilter(null);
            }
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     */
    public function setSlug($slug): void
    {
        $this->slug = $slug;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(?int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getSortOrder(): ?float
    {
        return $this->sort_order;
    }

    public function setSortOrder(?float $sort_order): self
    {
        $this->sort_order = $sort_order;

        return $this;
    }
}
