<?php

namespace App\Entity;

use App\Repository\UserLegalRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use App\Entity\User;

/**
 * @ORM\Entity(repositoryClass=UserLegalRepository::class)
 */
class UserLegal
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *
     */
    private $id;

    const MAIL = 1;
    const FEMALE = 2;


    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     */
    private $company_name;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    private $sex;

    /**
     * @ORM\Column(type="string")
     */
    private $surname;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Assert\Regex(pattern="/^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/", message="Неверный формат рабочего телефона")
     *
     */
    private $work_number;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $country;

    /**
     * @ORM\Column(type="integer", nullable=true, name="`index`")
     * @Assert\Length(max=6)
     */
    private $index;

    /**
     * @ORM\Column(type="string")
     */
    private $address;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Assert\Regex(pattern="/^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/", message="Неверный формат доп. рабочего телефона")
     *
     */
    private $work_number_additive;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Assert\Regex(pattern="/^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/", message="Неверный формат доп. мобильного телефона")
     *
     */
    private $person_number_additive;

    /**
     * @ORM\Column(type="boolean")
     *
     */
    private $show_work_number_additive;


    /**
     * @ORM\Column(type="boolean")
     *
     */
    private $show_person_number_additive;

    /**
     * @ORM\Column(type="bigint")
     * @Assert\NotBlank()
     * @Assert\Length(max="10")
     */
    private $INN;

    /**
     * @ORM\Column(type="string")
     */
    private $legal_address;


    /**
     * @ORM\OneToOne(targetEntity=User::class, mappedBy="user_legal", cascade={"persist", "remove"})
     */
    private $user_link;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getCompanyName()
    {
        return $this->company_name;
    }

    /**
     * @param mixed $company_name
     */
    public function setCompanyName($company_name): void
    {
        $this->company_name = $company_name;
    }

    /**
     * @return mixed
     */
    public function getSex()
    {
        return $this->sex;
    }

    /**
     * @param mixed $sex
     */
    public function setSex($sex): void
    {
        $this->sex = $sex;
    }

    /**
     * @return mixed
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param mixed $surname
     */
    public function setSurname($surname): void
    {
        $this->surname = $surname;
    }

    /**
     * @return mixed
     */
    public function getWorkNumber()
    {
        return $this->work_number;
    }

    /**
     * @param mixed $work_number
     */
    public function setWorkNumber($work_number): void
    {
        $this->work_number = $work_number;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country): void
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getIndex()
    {
        return $this->index;
    }

    /**
     * @param mixed $index
     */
    public function setIndex($index): void
    {
        $this->index = $index;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address): void
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getWorkNumberAdditive()
    {
        return $this->work_number_additive;
    }

    /**
     * @param mixed $work_number_additive
     */
    public function setWorkNumberAdditive($work_number_additive): void
    {
        $this->work_number_additive = $work_number_additive;
    }

    /**
     * @return mixed
     */
    public function getPersonNumberAdditive()
    {
        return $this->person_number_additive;
    }

    /**
     * @param mixed $person_number_additive
     */
    public function setPersonNumberAdditive($person_number_additive): void
    {
        $this->person_number_additive = $person_number_additive;
    }

    /**
     * @return mixed
     */
    public function getShowWorkNumberAdditive()
    {
        return $this->show_work_number_additive;
    }

    /**
     * @param mixed $show_work_number_additive
     */
    public function setShowWorkNumberAdditive($show_work_number_additive): void
    {
        $this->show_work_number_additive = $show_work_number_additive;
    }

    /**
     * @return mixed
     */
    public function getShowPersonNumberAdditive()
    {
        return $this->show_person_number_additive;
    }

    /**
     * @param mixed $show_person_number_additive
     */
    public function setShowPersonNumberAdditive($show_person_number_additive): void
    {
        $this->show_person_number_additive = $show_person_number_additive;
    }

    /**
     * @return mixed
     */
    public function getINN()
    {
        return $this->INN;
    }

    /**
     * @param mixed $INN
     */
    public function setINN($INN): void
    {
        $this->INN = $INN;
    }

    /**
     * @return mixed
     */
    public function getLegalAddress()
    {
        return $this->legal_address;
    }

    /**
     * @param mixed $legal_address
     */
    public function setLegalAddress($legal_address): void
    {
        $this->legal_address = $legal_address;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getUserLink()
    {
        return $this->user_link;
    }

    /**
     * @param mixed $user_link
     */
    public function setUserLink($user_link): void
    {
        $this->user_link = $user_link;
    }

}
