<?php

namespace App\Repository;

use App\Entity\Chat;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Chat|null find($id, $lockMode = null, $lockVersion = null)
 * @method Chat|null findOneBy(array $criteria, array $orderBy = null)
 * @method Chat[]    findAll()
 * @method Chat[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ChatRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Chat::class);
    }

    public function getChatByUser($userId) {
        return $this->createQueryBuilder('ch')
            ->where('ch.to_user = :user')
            ->orWhere('ch.from_user = :user')
            ->andWhere('ch.product IS NOT NULL ')
            ->setParameter('user', $userId)
            ->getQuery()
            ->getResult();
    }

    public function getChatByUserAndProduct($userId, $productId) {
        return $this->createQueryBuilder('ch')
            ->where('ch.to_user = :user')
            ->orWhere('ch.from_user = :user')
            ->andWhere('ch.product = :product ')
            ->setParameter('user', $userId)
            ->setParameter('product', $productId)
            ->getQuery()
            ->getResult();
    }

    public function getChatByAdmin($userId) {
        return $this->createQueryBuilder('ch')
            ->where('ch.to_user = :user')
            ->orWhere('ch.from_user = :user')
            ->andWhere('ch.product IS NULL ')
            ->setParameter('user', $userId)
            ->getQuery()
            ->getResult();
    }



    public function getAdminChats($userId) {
        return $this->createQueryBuilder('ch')
            ->leftJoin('ch.messages', 'm')
            ->where('ch.to_user = :user')
            ->orWhere('ch.from_user = :user')
            ->andWhere('ch.product IS NULL ')
            ->andWhere('m.author != :user')
            ->setParameter('user', $userId)
            ->getQuery()
            ->getResult();
    }


    // /**
    //  * @return Chat[] Returns an array of Chat objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Chat
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
