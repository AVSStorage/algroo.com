<?php

namespace App\Repository;

use App\Entity\FilterValues;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FilterValues|null find($id, $lockMode = null, $lockVersion = null)
 * @method FilterValues|null findOneBy(array $criteria, array $orderBy = null)
 * @method FilterValues[]    findAll()
 * @method FilterValues[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FilterValuesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FilterValues::class);
    }

    // /**
    //  * @return FilterValues[] Returns an array of FilterValues objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FilterValues
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
