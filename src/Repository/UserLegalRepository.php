<?php

namespace App\Repository;

use App\Entity\UserLegal;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserLegal|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserLegal|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserLegal[]    findAll()
 * @method UserLegal[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserLegalRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserLegal::class);
    }

    // /**
    //  * @return UserLegal[] Returns an array of UserLegal objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserLegal
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
